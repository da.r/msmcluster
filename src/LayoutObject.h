#ifndef MSMCLUSTER_LAYOUTOBJECT_H_INCLUDED
#define MSMCLUSTER_LAYOUTOBJECT_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "Utilities.h"

namespace megamol {
namespace msmcluster {

	/**
	* Object that represents images in a way that they can be used with
	* implementations of force directed layout and FastMap that are contained
	* in vislib.
	*/
	class LayoutObject {

	public:

		LayoutObject() = default;

		/**
		* Constructs a layout object with the given properties and level.
		*
		* @param imageProperties
		* @param level
		*/
		LayoutObject(ImageRepresentation imageProperties, ClusterLvl level);

		/**
		* Caculates the distance between this and the given other object.
		*
		* @param other another object
		*
		* @return distance between objects
		*/
		float Distance(LayoutObject &other);

		/**
		* Compares this object with the other object.
		*
		* @return true if objects have the same properties
		*/
		inline bool LayoutObject::operator ==(const LayoutObject &rhs) {
			return imageProperties == rhs.imageProperties;
		}

		/**
		* Total weight of the edges connected to this instance of layout object.
		*/
		float TotalWeight(void) { return 1.0f; }

		/**
		* Returns weight of the edge between this object and the given 
		* other object.
		*
		* @return edge weight
		*/
		float Weight(LayoutObject &other);


	private:
		ImageRepresentation imageProperties;
		ClusterLvl level;

	};


} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_LAYOUTOBJECT_H_INCLUDED */