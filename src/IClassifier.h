#ifndef MSMCLUSTER_ICLASSIFIER_H_INCLUDED
#define MSMCLUSTER_ICLASSIFIER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include <dlib/clustering.h>
#include "Utilities.h"


namespace megamol {
namespace msmcluster {

	using Cluster_t = std::vector< ImageRepresentation >;

	/**
	* Interface of the classifier classes 
	*/
	class IClassifier {

	public:

		/**
		* Getter for a vector that contains ImageRepresentations of all images in
		* a cluster with the given cluster id.
		*
		* @return numOfClusters
		*/
		virtual std::vector<ImageRepresentation> GetCluster(int clusterID) = 0;

		/**
		* Getter for a vector of clusters.
		*
		* @return two dimensional vector that constains clusters
		*/
		virtual std::vector<Cluster_t> GetClusters() = 0;

		/**
		Getter for the number of clusters

		@return number of clusters
		*/
		virtual int GetNumOfClusters() = 0;
		
		/**
		* Returns the representation of the representative image of a cluster with
		* the given clusterID.
		*
		* @param clusterID ID of the cluster
		* @return representation of the representative image of the cluster
		*/
		virtual ImageRepresentation GetRepresentativeImage(int clusterID) = 0;
		
		/**
		* Starts the clustering
		*/
		virtual void Run() = 0;

	};


} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_ICLASSIFIER_H_INCLUDED */