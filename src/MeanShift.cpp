#include "stdafx.h"

#include "MeanShift.h"
#include <cstdlib>
#include <limits>
#include <random>
#include <fstream>

namespace megamol {
namespace msmcluster {


		/**
		* MeanShift::MeanShift
		*/
		MeanShift::MeanShift(std::string folder) {
			// Check whether the output of clustering already exists
			std::string outputFilename = folder + std::string("output.csv");
			std::ifstream inf(outputFilename);
			if (!inf.good()) {
				// Relative paths with comvi submodule
				system(std::string("python ..\\plugins\\MSMCluster\\comvi\\programming\\main.py --path " + folder + " --output " + folder).c_str());
			}
			readOutputFile(outputFilename);
		}


		/**
		* MeanShift::GetCluster
		*/
		std::vector<ImageRepresentation> MeanShift::GetCluster(int clusterID) {
			return clusters.at(clusterID);
		}


		/**
		* MeanShift::GetClusters
		*/
		std::vector<Cluster_t > MeanShift::GetClusters() {
			return clusters;
		}


		/**
		* MeanShift::GetNumOfClusters
		*/
		int MeanShift::GetNumOfClusters() {
			return clusters.size();
		}


		/**
		* MeanShift::GetRepresentativeImage
		*/
		ImageRepresentation MeanShift::GetRepresentativeImage(int clusterID) {
			return representativeImages[clusterID];
		}


		/**
		* MeanShift::readOutputFile
		*/
		void MeanShift::readOutputFile(std::string filename) {
			std::ifstream inf(filename);
			if (!inf) {
				std::cerr << "Could not read " << filename << std::endl;
				return;
			}


			while (inf) {
				std::string path;
				std::getline(inf, path, ',');

				// If there is no path line is probably emtpy
				if (path.length() == 0)
					continue;

				if (path.substr(0, 5) == "BEGIN") {
					return;
				}

				std::string strPosX, strPosY;
				std::vector<double> position;
				std::getline(inf, strPosX, ',');
				position.push_back(std::stod(strPosX));
				std::getline(inf, strPosY, ',');
				position.push_back(std::stod(strPosY));
				std::string strClusterID;
				std::getline(inf, strClusterID);

				int clusterID = std::stoi(strClusterID);
				int clustersToAdd = clusterID - (clusters.size() - 1);
				if (clustersToAdd > 0) {
					for (int i = 0; i < clustersToAdd; ++i)
						clusters.push_back(Cluster_t());
				}
				clusters[clusterID].push_back(ImageRepresentation{ path, position });

			}
		}


		/**
		* MeanShift::Run
		*/
		void MeanShift::Run() {
			// To pick a random element from the cluster
			std::random_device randDev;
			std::mt19937 randEngine(randDev());
			// Store random images as representative images
			for (int clus = 0; clus < clusters.size(); ++clus) {
				int randIdx = std::uniform_int_distribution<>(0, clusters[clus].size() - 1)(randEngine);
				representativeImages.push_back(clusters[clus][randIdx]);
			}
		}


} /* end namespace msmcluster */
} /* end namespace megamol */