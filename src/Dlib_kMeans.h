#ifndef MSMCLUSTER_DLIB_KMEANS_H_INCLUDED
#define MSMCLUSTER_DLIB_KMEANS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include <dlib/clustering.h>
#include "IClassifier.h"
#include "Utilities.h"

namespace megamol {
namespace msmcluster {

		using Cluster_t = std::vector<ImageRepresentation>;

		/**
		* Wrapper class around the dlib implementation of bottom up agglomerative
		* clustering
		*/
		class Dlib_kMeans : public IClassifier {

		public:

			/**
			* Takes path to the data and K, number of wanted clusters.
			* Initializes the classifier.
			*
			* @param data data to cluster
			* @param K number of clusters
			*/
			Dlib_kMeans(std::vector<ImageRepresentation> &data, int K);

			/**
			* Getter for a vector that contains ImageRepresentations of all images in
			* a cluster with the given cluster id.
			*
			* @return numOfClusters
			*/
			std::vector<ImageRepresentation> GetCluster(int clusterID);

			/**
			* Getter for a vector of clusters.
			*
			* @return two dimensional vector that constains clusters
			*/
			std::vector<Cluster_t > GetClusters();

			/**
			Getter for the number of clusters

			@return number of clusters
			*/
			int GetNumOfClusters();

			/**
			* Returns the representation of the representative image of a cluster with
			* the given clusterID.
			*
			* @param clusterID ID of the cluster
			* @return representation of the representative image of the cluster
			*/
			ImageRepresentation GetRepresentativeImage(int clusterID);

			/**
			* Starts the clustering
			*/
			void Run();

		private:
			typedef dlib::matrix<double, 0, 1> SampleType;

			int K;
			std::vector<ImageRepresentation> data;
			std::vector<SampleType> samples;
			std::vector<SampleType> centroids;
			std::vector<Cluster_t> clusters;
			std::vector<ImageRepresentation> nearestToCentroid;

		};

} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_DLIB_KMEANS_H_INCLUDED */