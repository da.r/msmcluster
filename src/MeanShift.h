#ifndef MSMCLUSTER_MEANSHIFT_H_INCLUDED
#define MSMCLUSTER_MEANSHIFT_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include <dlib/clustering.h>
#include "IClassifier.h"
#include "Utilities.h"

namespace megamol {
namespace msmcluster {

		using Cluster_t = std::vector<ImageRepresentation>;

		class MeanShift : public IClassifier {

		public:

			/**
			* Creates and instance of MeanShift classifier. Starts the comvi
			* program with the given folder and reads its output.
			* Comvi should be initialized in MSMCluster plugin directory for 
			* this classifier to work.
			*
			* @param folder folder that contains images
			*/
			MeanShift(std::string folder);

			/**
			* Getter for a vector that contains ImageRepresentations of all images in
			* a cluster with the given cluster id.
			*
			* @return numOfClusters
			*/
			std::vector<ImageRepresentation> GetCluster(int clusterID);

			/**
			* Getter for a vector of clusters.
			*
			* @return two dimensional vector that constains clusters
			*/
			std::vector<Cluster_t > GetClusters();

			/**
			Getter for the number of clusters

			@return number of clusters
			*/
			int GetNumOfClusters();

			/**
			* Returns the representation of the representative image of a cluster
			* with the given clusterID. Random image from the cluster is chosen as
			* its representative image.
			*
			* @param clusterID ID of the cluster
			* @return representation of the representative image of the cluster
			*/
			ImageRepresentation GetRepresentativeImage(int clusterID);

			/**
			* Only chooses representative images
			*/
			void Run();

		private:
			std::vector<Cluster_t> clusters;
			std::vector<ImageRepresentation> representativeImages;

			/**
			* Reads an output file and stores content into the member variables.
			*
			* @param filename name of the output file
			*/
			void readOutputFile(std::string filename);
		};

} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_MEANSHIFT_H_INCLUDED */