#ifndef MSMCLUSTER_CLUSTERRENDERER_H_INCLUDED
#define MSMCLUSTER_CLUSTERRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */


#include "mmcore/view/View3D.h"
#include "mmcore/view/AbstractCamParamSync.h"
#include "mmcore/view/Renderer2DModule.h"
#include "mmcore/Call.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/ParamSlot.h"
#include "mmcore/view/Call6DofInteraction.h"
#include "mmcore/view/CallCamParamSync.h"
#include "vislib/graphics/gl/GLSLShader.h"
#include "vislib/graphics/gl/CameraOpenGL.h"
#include "vislib/math/ForceDirected.h"
#include "vislib/Array.h"


#include <queue>

#include "BottomUpAgglomerative.h"
#include "Dlib_kMeans.h"
#include "KMeans.h"
#include "MeanShift.h"
#include "LayoutObject.h"
#include "TextureArray.h"




namespace megamol {
namespace msmcluster {


	/**
     * Renderer for the 2D clusters.
     */
	class ClusterRenderer : public core::view::Renderer2DModule,
		public core::view::AbstractCamParamSync {
	public:

        /**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void) {
            return "ClusterRenderer";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void) {
            return "Renderer 2D MSM clusters.";
        }

        /**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
        static bool IsAvailable(void) {
            return true;
        }

		/** Dtor. */
		virtual ~ClusterRenderer(void);

		/** Ctor. */
		ClusterRenderer(void);

    protected:

        /**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);

        /**
         * The get capabilities callback. The module should set the members
         * of 'call' to tell the caller its capabilities.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetCapabilities(core::Call& call);

        /**
         * The get extents callback. The module should set the members of
         * 'call' to tell the caller the extents of its data (bounding boxes
         * and times).
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetExtents(megamol::core::view::CallRender2D& call);

		/**
         * Callback for mouse events (move, press, and release)
         *
         * @param x The x coordinate of the mouse in world space
         * @param y The y coordinate of the mouse in world space
         * @param flags The mouse flags
         */
		virtual bool MouseEvent(float x, float y, ::megamol::core::view::MouseFlags flags);

		/**
		* Handles a request for the camera parameters used by the view.
		*
		* @param c The call being executed.
		*
		* @return true in case of success, false otherwise.
		*/
		virtual bool OnGetCamParams(megamol::core::view::CallCamParamSync& c);

        /**
         * Implementation of 'Release'.
         */
        virtual void release(void);

        /**
         * The render callback.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool Render(megamol::core::view::CallRender2D& call);

	private:

		using Cluster_t = std::vector< std::pair<std::string, std::vector<double> > >;

		struct ImageRectangle {
			GLfloat width;
			GLfloat height;

			ImageRectangle() = default;

			ImageRectangle(GLfloat w, GLfloat h) : width(w), height(h) {}

			ImageRectangle operator*(float scalar) const {
				return ImageRectangle(
					scalar * width,
					scalar * height
				);
			}
		};
		

		struct ImageInfo {
			std::string name;
			std::string	path;
			int idx;
			std::string parentName;
			std::string parentPath;
			int parentIdx;	

			ClusterLvl level;
			bool subclustered;


			int texArrayIdx;
			int inTexArrIdx;

			GLubyte* ptrImage;

			ImageInfo() = default;

			ImageInfo(std::string name, std::string path, int idx, std::string parentName,
				std::string parentPath, int parentIdx, ClusterLvl level, bool subclustered, 
				int texArrayIdx = -1, int inArrayIdx = -1, GLubyte *image = nullptr)
				: name(name), path(path), idx(idx), parentName(parentName), parentPath(parentPath),
				parentIdx(parentIdx), level(level), subclustered(subclustered), 
				texArrayIdx{ texArrayIdx }, inTexArrIdx{ inArrayIdx }, ptrImage(image) {}
		};

		struct TexCache {
			GLubyte* image;
			int width;
			int height;
			int channels;
			int texArrayIdx;
			int inArrayIdx;

			TexCache(GLubyte* image, int width, int height, int channels, int texArrayIdx, int inArrayIdx)
				: image{ image }, width{ width }, height{ height }, texArrayIdx{ texArrayIdx }, inArrayIdx{ inArrayIdx } {}
		};

		
		// Parameters
		
		bool allChannels;
		core::param::ParamSlot paramImageRepresentation;

		core::param::ParamSlot paramClusteringAlgo;

		int numberOfClusters;
		core::param::ParamSlot paramNumOfClusters;

		int lvl2NumOfClusters = 2;
		core::param::ParamSlot paramLvl2NumOfClusters;

		core::param::ParamSlot paramCluster;

		core::param::ParamSlot paramImagesPath;

		GLfloat xScale = 1.0f;
		core::param::ParamSlot paramXScale;

		GLfloat yScale = 1.0f;
		core::param::ParamSlot paramYScale;

		GLfloat zScale = 1.0f;
		core::param::ParamSlot paramZScale;


		GLfloat zoomingSensitivity = 0.2f;
		core::param::ParamSlot paramZoomingSensitivity;

		core::param::ParamSlot paramZoomIn;

		core::param::ParamSlot paramZoomOut;

		float imageSizeScalling = 1.0f;
		core::param::ParamSlot paramImageSizeScalling;

		float lvl2ScaleImageSize = 0.6f;
		core::param::ParamSlot paramLvl2ScaleImageSize;

		core::param::ParamSlot paramSelectedImage;

		float currentEnergy = std::numeric_limits<float>::max();
		core::param::ParamSlot paramCurrentEnergy;

		
		std::vector<float> currentEnergysLvl2;
		// core::param::ParamSlot paramCurrentEnergyLvl2;

		float stopEnergy = 0.0000002;
		core::param::ParamSlot paramStopEnergy;

		float stopEnergyLvl2 = 0.0000002;
		// core::param::ParamSlot paramStopEnergyLvl2;
	
		float damping = 0.1f;
		core::param::ParamSlot paramDamping;

		float stepLength= 0.1f;
		core::param::ParamSlot paramStepLength;


		int texImgCacheSizeLimit = 100;
		core::param::ParamSlot paramTexImgCacheSizeLimit;
	
		// Params related to the uniform grid layout screen
		core::param::ParamSlot paramClusterName;
		core::param::ParamSlot paramBack;

		/**
		* The buttons that must be pressed on the 6DOF device to select a
		* cluster.
		*/
		core::param::ParamSlot paramSelectionButtons;

		// Clustering 
		std::unique_ptr<IClassifier> classifier;
		// string denotes a representative of the parent cluster and boolean denotes its level
		// (level is needed because the representative image of subcluster can be the same as 
		// the representative image of the parent cluster)
		std::map<std::pair<std::string, ClusterLvl>, std::vector< megamol::msmcluster::ImageRepresentation> > clustersList;
		
		
		// Interaction

		/**
		* Specifies the index of the view that shows the border in
		* 'paramSelectionColour'.
		*/
		core::param::ParamSlot paramHighlightedView;

		core::param::ParamSlot paramPhysicalHeight;
		core::param::ParamSlot paramPhysicalOrigin;
		core::param::ParamSlot paramPhysicalPointerIdle;
		core::param::ParamSlot paramPhysicalWidth;

		// int represents an index in imageDestinations
		std::pair<bool, int> drag = std::make_pair(false, -1);

		// Correcting for the picking position on the image
		float xCorrection;
		float yCorrection;

		// To block doing the same clustering at the same time (e.g. when the user 
		// hits Cluster button multiple times in a row)
		bool doingClustering = false;
		
		// Bool indicates whether subclustering should be done.
		// Int denotes the cluster whose representative image was clicked. 
		std::pair<bool, int> clusterToSubcluster;
	
		// Zooming
		GLfloat bboxMinX = -2.0f, bboxMinY = -1.0f, bboxMaxX = 2.0f, bboxMaxY = 1.0f;
		bool bboxSizeChanged = false;
		ImageRectangle origImgRect;
		ImageRectangle currentImgRect;
		// To translate the bounding box after resetting the view
		GLfloat previousModelViewX;
		GLfloat previousModelViewY;
		GLfloat toMoveX = 0.0f, toMoveY = 0.0f;
		// debug - a hack to make the bbox move with the scene
		// Zoom in increases it, zoom out decreases it
		int diffZoomInOut = 0;


		
		
		// Visualization
		
		vislib::graphics::gl::GLSLShader shaderProgram;
		GLuint VBO, VAO;
		std::vector<GLfloat> origVertices;
		std::vector<GLfloat> currentVertices;
		GLuint sVBO, sVAO;
		std::vector<GLfloat> currentLvl2Vertices;
		std::vector<GLuint> indices;
		GLfloat modelViewMatrix[16];
		GLfloat projectionMatrix[16];
		std::vector<vislib::math::Vector<float, 3> > clusterColors;

		// Position of image descriptors in the image descriptor space
		// Array corresponds to the instances of force directed layout
		vislib::Array<LayoutObject> imagesSource;
		std::vector<vislib::Array<LayoutObject> > imagesSourceLvl2;
		// Position of images in bounding box (images of level 1)
		// Instance of force directed layout updates it continuously
		vislib::Array<vislib::math::Vector<float, 2> > imagesDestination;
		// Positions of images that are continuously updated by the corresponding instance 
		// of force directed layout
		std::vector<vislib::Array<vislib::math::Vector<float, 2> > > imagesDestinationsLvl2;

		// Contains information about images that should be drawn
		std::vector<ImageInfo> imagesToDraw;
		// Destinations of images drawn in the bounding box (idx corresponds to idx in imagesToDraw)
		std::vector<vislib::math::Vector<float, 2> *> drawingImagesDestination;
		
		std::unique_ptr<vislib::math::ForceDirected<LayoutObject, float, 2> > activeforceDirected;
		std::vector<std::unique_ptr<vislib::math::ForceDirected<LayoutObject, float, 2> > > forceDirectedLayouts;
		
		// Warning should be printed if the wanted number of clusters is larger than 
		// number of images in the given directory
		bool warningMessage = false;


		// Grid layout 

		// Boolean is true when the images of some clusters should be drawn
		// int represents the id of the selected cluster
		std::pair<bool, int> drawImagesInCluster = std::make_pair(false, -1);

		// Variables used to store the current state of the force directed layout
		// when changin to grid layout
		std::vector<vislib::math::Vector<float, 2> *> storedImagesDestination;
		std::vector<ImageInfo> storedImagesToDraw;
		std::vector<GLfloat> storedOrigVertices;
		ImageRectangle gridRectangle;
		bool gridDrawn = false;
				
		
		


		// Caching
		
		// contains paths to the corresponding entries in texImgCache
		std::vector<std::string> texImgCacheNames;
		std::vector<TexCache> texImgCache;
		std::vector<TextureArray> texArrays;

		std::string cacheFileNameGray = "cache_gray.msmcluster";
		std::string cacheFileNameColors = "cache_colors.msmcluster";

		// Cached image moments that are read in
		std::vector<ImageRepresentation > readInCache;

		/** Enables the view receiving updates from a 6DOF pointing device. */
		megamol::core::CallerSlot slotGet6DofState;
		

		/**
		* Loads an image with the given name on the given index in texture cache.
		* Returns false if it is not successful. Sets indexes related to textures 
		* in the given ImageInfo object.
		*
		* @param imgInfo ImageInfo object that is modified
		* @param idx index of the image, used to print which image should 
		* currently be loaded
		*
		* @return true if successful 
		*/
		bool megamol::msmcluster::ClusterRenderer::checkLoadTexCache(ImageInfo &imgInfo, int idx);
		
		/**
		* Reads in the images, writes those that were not in cache to cache.
		* Clusters the data, initializes texture array and initializes 
		* instance of force directed layout.
		*/
		void clusterAndInitializeTextureArray();

		/**
		* Draws the image with the given index in imagesToDraw.
		*
		* @param i index of the correponding ImageInfo in imagesToDraw
		*/
		void drawImage(int i);

		/**
		* Invoke 'slotGet6DofState' to retrieve the current state of the 6DOF
		* device.
		*
		* @return The call or nullptr in case of an error.
		*/
		megamol::core::view::Call6DofInteraction* get6DofState(void);

		/**
		* Loads the image on the given path. Stores information to texWidth, 
		* texHeight and numTexChannels. It also adds index of the texture array
		* and index of image within that texture array to the given 
		* image info object.
		*
		* @param pathToImage
		* @param idx index of the loaded image, used to print which image should 
		* currently be loaded
		* @param imInfo ImageInfo object that should be modified
		* @param texWidth width of the loaded image
		* @param texHeight height of the loaded image
		* @param numTexChannels number of channels that the loaded image has
		*
		* @return loaded image
		*/
		GLubyte* loadImage(std::string pathToImage, int idx, ImageInfo &imInfo, int &texWidth, 
			int &texHeight, int &numTexChannels);

		/**
		* Callback function for the back key (returns from grid layout to force directed
		* layout)
		*/
		bool onParamBackPressed(megamol::core::param::ParamSlot &paramSlot);

		/**
		* Callback function for the Cluster key.
		*/
		bool onParamClusterPressed(megamol::core::param::ParamSlot &paramSlot);

		/**
		* Callback function for the zoomIn key
		*/
		bool onParamZoomInPressed(megamol::core::param::ParamSlot &paramSlot);

		/**
		* Callback function for the zoomOut key
		*/
		bool onParamZoomOutPressed(megamol::core::param::ParamSlot &paramSlot);

		/**
		* Loads images, manages texture cache and stores information about those images into
		* a vector.
		*
		* @param parentName name of the parent image
		* @param parentPath path to the parent image
		* @param parentIdx parent index in imagesToDraw
		* @param clustering level
		*
		*/
		void setUpTextures(int numOfClusters, std::string parentName, std::string parentPath,
			int parentIdx, ClusterLvl level);

		/**
		* Clusters the images in some cluster and initializes the corresponding 
		* instance of force directed layout.
		*
		* @param parent name of the representative image of the cluster that should be
		* sublustered
		* @param parentPath path to the representative image of the parent cluster
		* @param parentIdx index in imagesToDraw of the representative image of the 
		* parent cluster
		*/
		void subcluster(std::string parent, std::string parentPath, int parentIdx);

		/* The Intercetion of the Powerwall*/
		vislib::graphics::SceneSpacePoint2D get6DofIntersection(
			const core::view::Call6DofInteraction *c);


		/** The scene camera */
		vislib::graphics::gl::CameraOpenGL cam;

		/** The normal camera parameters */
		vislib::SmartPtr<vislib::graphics::CameraParameters> camParams;

		/** oldTracking Pos */
		vislib::math::Point<vislib::graphics::SceneSpaceType, 3U> oldPos;

		
		/**the old TrackingButtonState */ 
		uint32_t oldTrackingButtonStates;


	};


	} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_CLUSTERRENDERER_H_INCLUDED */