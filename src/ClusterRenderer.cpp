#include "stdafx.h"

#include "ClusterRenderer.h"


// MegaMol includes
#include "mmcore/CoreInstance.h"
#include "mmcore/param/ButtonParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/FilePathParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/view/CallRender2D.h"


// vislib includes
#include "vislib/graphics/BitmapCodecCollection.h"
#include "vislib/sys/KeyCode.h"
#include "vislib/math/Cuboid.h"
#include "vislib/math/Dimension.h"
#include "vislib/math/Matrix4.h"
#include "vislib/math/Plane.h"
#include "vislib/math/Point.h"
#include "vislib/math/Rectangle.h"
#include "vislib/math/Vector.h"
#include "mmcore/param/Vector3fParam.h"


// other includes
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <algorithm>
#include <filesystem>

#include <future>
#include <cmath>
#include <map>
#include <memory>


using namespace megamol::msmcluster;

/*
* ClusterRenderer::~ClusterRenderer
*/
megamol::msmcluster::ClusterRenderer::~ClusterRenderer() {
	this->Release();
}


/*
* ClusterRenderer::ClusterRenderer
*/
megamol::msmcluster::ClusterRenderer::ClusterRenderer() :megamol::core::view::Renderer2DModule(),
paramImageRepresentation("clustering::imageRepresentation", "Allows the user to choose the internal representation of the image used when clustering."),
paramClusteringAlgo("clustering::clusteringAlgo", "Allows the user to choose the clustering algorithm."),
paramImagesPath("clustering::imagesPath", "Path to the folder that contains the images."),
paramNumOfClusters("clustering::numberOfClusters", "Data is clustered in this number of clusters."),
paramLvl2NumOfClusters("clustering::numberOfClustersOnLvl2", "Cluster is further clustered into this number of clusters."),
paramCluster("clustering::cluster", "Cluster the data again"),
paramXScale("Powerwall x-Faktor", "The scale of Phsicalwidth"),
paramYScale("Powerwall y-Faktor", "The scale of Phsicalhight"),
paramZScale("Powerwall z-Faktor", "The scale of Phsicaldepth"),
paramImageSizeScalling("visualization::imageSizeScalling", "Change the image size. Width and height of every image is multiplied by this value."),
paramLvl2ScaleImageSize("visualization::subclusterImageSize", "Scales every image that represents a cluster within a cluster with respect to the size of its parent cluster."),
paramZoomingSensitivity("visualization::zoomingSensitivity", "Zooming step size. Default value is 0.2."),
paramZoomIn("visualization::zoomIn", "Zooms in."),
paramZoomOut("visualization::zoomOut", "Zooms out."),
paramSelectedImage("visualization::selectedImage", "Image that is selected"),
paramCurrentEnergy("visualization::layout::currentEnergy", "1000000 x Current energy of the layout is shown here."),
paramStopEnergy("visualization::layout::stopEnergy", "1000000 x Energy at which the layout calculation stops."),
paramDamping("visualization::layout::damping", "Damping factor applied to forces for quicker becalming of system."),
paramStepLength("visualization::layout::stepLength", "Integration step length."),
paramSelectionButtons("selection::buttons", "The button mask for selecting clusters via a 6 DOF device."),
paramTexImgCacheSizeLimit("caching::textureCacheSize", "Number of textures that can be cached."),
paramClusterName("uniformGrid::whichCluster", "Representative image of the cluster whose images are shown."),
paramBack("uniformGrid::back", "Return to the force directed layout."),
slotGet6DofState("get6DofState", "Retrieves updates from a 6 DOF device."),
paramHighlightedView("Highlighted View", "The view of the 6DOF Device"),
paramPhysicalHeight("Physical Height", "Represents the physical height"),
paramPhysicalOrigin("Physical Origin", "The origin of the physical view as 3D Vector"),
paramPhysicalPointerIdle("Physical pointIdle", "The idle orientation of the 6 DOF Device"),
paramPhysicalWidth("Physical width", "The total physical width of the view") {

	this->oldTrackingButtonStates = 0x00;
	this->camParams = this->cam.Parameters();
	this->camParams->SetVirtualViewSize(10800.0f, 4096.0f);
	this->MakeSlotAvailable(&this->slotGetCamParams);
	this->MakeSlotAvailable(&this->slotSetCamParams);

	core::param::EnumParam *representations = new core::param::EnumParam(0);
	representations->SetTypePair(InternalRepresentation::GRAYSCALE, "imageMoments_grayscale");
	representations->SetTypePair(InternalRepresentation::ALL_CHANNELS, "imageMoments_allColorChannels");
	this->paramImageRepresentation << representations;
	this->MakeSlotAvailable(&this->paramImageRepresentation);

	core::param::EnumParam *algos = new core::param::EnumParam(0);
	algos->SetTypePair(ClusterAlgo::KMEANS, "kMeans");
	algos->SetTypePair(ClusterAlgo::DLIB_KMEANS, "dlib::kMeans");
	algos->SetTypePair(ClusterAlgo::BOTTOM_UP_AGGLOMERATIVE, "dlib::bottomUpAgglomerative");
	algos->SetTypePair(ClusterAlgo::MEAN_SHIFT, "MeanShift (in python)");
	this->paramClusteringAlgo << algos;
	this->MakeSlotAvailable(&this->paramClusteringAlgo);

	this->paramImagesPath << new core::param::FilePathParam("../plugins/MSMCluster/resources/");
	this->MakeSlotAvailable(&this->paramImagesPath);

	this->paramNumOfClusters << new core::param::IntParam(0x04);
	this->MakeSlotAvailable(&this->paramNumOfClusters);

	this->paramLvl2NumOfClusters << new core::param::IntParam(0x02);
	this->MakeSlotAvailable(&this->paramLvl2NumOfClusters);

	this->paramCluster << new core::param::ButtonParam(vislib::sys::KeyCode::KEY_ENTER);
	this->paramCluster.SetUpdateCallback(&ClusterRenderer::onParamClusterPressed);
	this->MakeSlotAvailable(&this->paramCluster);

	this->paramImageSizeScalling << new core::param::FloatParam(0x01);
	this->MakeSlotAvailable(&this->paramImageSizeScalling);

	this->paramLvl2ScaleImageSize << new core::param::FloatParam(lvl2ScaleImageSize);
	this->MakeSlotAvailable(&this->paramLvl2ScaleImageSize);

	this->paramXScale << new core::param::FloatParam(1.0);
	this->MakeSlotAvailable(&this->paramXScale);


	this->paramYScale << new core::param::FloatParam(1.0);
	this->MakeSlotAvailable(&this->paramYScale);


	this->paramZScale << new core::param::FloatParam(1.0);
	this->MakeSlotAvailable(&this->paramZScale);

	this->paramZoomingSensitivity << new core::param::FloatParam(0.02);
	this->MakeSlotAvailable(&this->paramZoomingSensitivity);

	this->paramZoomIn << new core::param::ButtonParam();
	this->paramZoomIn.SetUpdateCallback(&ClusterRenderer::onParamZoomInPressed);
	this->MakeSlotAvailable(&this->paramZoomIn);

	this->paramZoomOut << new core::param::ButtonParam();
	this->paramZoomOut.SetUpdateCallback(&ClusterRenderer::onParamZoomOutPressed);
	this->MakeSlotAvailable(&this->paramZoomOut);

	this->paramSelectedImage << new core::param::StringParam("no image selected");
	this->MakeSlotAvailable(&this->paramSelectedImage);

	this->paramCurrentEnergy << new core::param::FloatParam(9999999.999);
	this->MakeSlotAvailable(&this->paramCurrentEnergy);

	// We have to multiply it because there are only three decimal places
	this->paramStopEnergy << new core::param::FloatParam(stopEnergy * 1000000);
	this->MakeSlotAvailable(&this->paramStopEnergy);

	this->paramDamping << new core::param::FloatParam(damping);
	this->MakeSlotAvailable(&this->paramDamping);

	this->paramStepLength << new core::param::FloatParam(stepLength);
	this->MakeSlotAvailable(&this->paramStepLength);

	this->paramTexImgCacheSizeLimit << new core::param::IntParam(texImgCacheSizeLimit);
	this->MakeSlotAvailable(&this->paramTexImgCacheSizeLimit);

	this->paramClusterName << new core::param::StringParam("not in uniform grid");
	this->MakeSlotAvailable(&this->paramClusterName);

	this->paramBack << new core::param::ButtonParam(vislib::sys::KeyCode::KEY_BACKSPACE);
	this->paramBack.SetUpdateCallback(&ClusterRenderer::onParamBackPressed);
	this->MakeSlotAvailable(&this->paramBack);

	// Powerwall-Scale
	this->paramHighlightedView << new core::param::IntParam(-1);
	this->MakeSlotAvailable(&this->paramHighlightedView);

	this->paramPhysicalHeight << new core::param::FloatParam(0, 0, FLT_MAX);
	this->MakeSlotAvailable(&this->paramPhysicalHeight);

	this->paramPhysicalOrigin << new core::param::Vector3fParam(
		vislib::graphics::SceneSpaceVector3D());
	this->MakeSlotAvailable(&this->paramPhysicalOrigin);

	this->paramPhysicalPointerIdle << new core::param::Vector3fParam(
		vislib::graphics::SceneSpaceVector3D(0, 0, -1));
	this->MakeSlotAvailable(&this->paramPhysicalPointerIdle);

	this->paramPhysicalWidth << new core::param::FloatParam(0, 0, FLT_MAX);
	this->MakeSlotAvailable(&this->paramPhysicalWidth);

	// Activate the param slot for the selection button.
	this->paramSelectionButtons << new core::param::IntParam(0x03);
	this->MakeSlotAvailable(&this->paramSelectionButtons);

	// Activate the param slot for the tracking system.
	this->slotGet6DofState.SetCompatibleCall<
		megamol::core::view::Call6DofInteractionDescription>();
	this->MakeSlotAvailable(&this->slotGet6DofState);
}


/*
* ClusterRenderer::checkLoadTexCache
*/
bool megamol::msmcluster::ClusterRenderer::checkLoadTexCache(ImageInfo &imgInfo, int idx) {
	auto cacheNamesIter = std::find(texImgCacheNames.begin(), texImgCacheNames.end(), imgInfo.path);

	if (cacheNamesIter != texImgCacheNames.end()) {

		int pos = cacheNamesIter - texImgCacheNames.begin();

		texArrays[texImgCache[pos].texArrayIdx].ImageToStore(texImgCacheNames[pos], texImgCache[pos].image);
		imgInfo.texArrayIdx = texImgCache[pos].texArrayIdx;
		imgInfo.inTexArrIdx = texArrays[texImgCache[pos].texArrayIdx].GetIdxInStorage();

		std::cout << "Image " << idx << " " << imgInfo.name << " is in cache" << std::endl;

		return true;
	}

	return false;
}


/*
* ClusterRenderer::clusterAndInitializeTextureArray
*/
void megamol::msmcluster::ClusterRenderer::clusterAndInitializeTextureArray() {
	std::vector<ImageRepresentation> vec_representations;
	

	// Check the chosen internal representation of images
	auto internalRepresentation = this->paramImageRepresentation.Param<core::param::EnumParam>()->Value();
	int internalReprDimensions;
	std::string cacheFileName = cacheFileNameGray;
	switch (internalRepresentation) {
	case InternalRepresentation::GRAYSCALE:
		allChannels = false;
		internalReprDimensions = 10;
		break;
	case InternalRepresentation::ALL_CHANNELS:
		allChannels = true;
		cacheFileName = cacheFileNameColors;
		internalReprDimensions = 30;
		break;
	default:
		allChannels = false;
	}

	// Read cached image descriptors if they exist
	readInCache.clear();
	readCache(cacheFileName, internalReprDimensions, readInCache);

	// Reading images and storing their vector representations in std::vector

	std::string directory = this->paramImagesPath.Param<core::param::FilePathParam>()->Value();

	std::vector<ImageRepresentation> wereNotInCache = readImagesConvertToImageDescriptors(directory, allChannels, vec_representations, readInCache);

	writeToCache(wereNotInCache, cacheFileName, internalReprDimensions);

	readInCache.insert(readInCache.end(), wereNotInCache.begin(), wereNotInCache.end());
	vec_representations.insert(vec_representations.end(), wereNotInCache.begin(), wereNotInCache.end());

	core::param::IntParam *atNumOfClustersParam = this->paramNumOfClusters.Param<core::param::IntParam>();
	// Handle the case where the numOfClusters > number of images
	if (numberOfClusters > vec_representations.size()) {
		numberOfClusters = vec_representations.size();
		atNumOfClustersParam->SetValue(numberOfClusters);
		warningMessage = true;
	}


	std::cout << "Finished Reading" << std::endl;

	// Check which algo is chosen
	auto clusterAlgo = this->paramClusteringAlgo.Param<core::param::EnumParam>()->Value();

	switch (clusterAlgo) {
	case ClusterAlgo::KMEANS:
		classifier.reset(new KMeans(vec_representations, numberOfClusters));
		break;
	case ClusterAlgo::DLIB_KMEANS:
		classifier.reset(new Dlib_kMeans(vec_representations, numberOfClusters));
		break;
	case ClusterAlgo::BOTTOM_UP_AGGLOMERATIVE:
		classifier.reset(new BottomUpAgglomerative(vec_representations, numberOfClusters));
		break;
	case ClusterAlgo::MEAN_SHIFT:
		classifier.reset(new MeanShift(directory));
		numberOfClusters = classifier->GetNumOfClusters();
		atNumOfClustersParam->SetValue(numberOfClusters);
		break;
	default:
		// temporarily
		classifier.reset(new KMeans(vec_representations, numberOfClusters));
	}

	clusterColors = computeColors(numberOfClusters);

	clustersList.clear();

	classifier->Run();
	for (int i = 0; i < classifier->GetClusters().size(); ++i) {
		clustersList[std::make_pair(classifier->GetRepresentativeImage(i).path, ClusterLvl::LEVEL1)] = classifier->GetCluster(i);
	}

	for (int i = 0; i < classifier->GetNumOfClusters(); ++i) {
		std::cout << "Representative image of cluster " << i + 1 << ": " << classifier->GetRepresentativeImage(i).path << std::endl;
	}

	// Clear what is left from last layout instance
	imagesToDraw.clear();
	for (auto &texArray : texArrays) {
		texArray.ResetStorage(texImgCacheNames);
	}


	// there are no parents for level 1
	setUpTextures(numberOfClusters, "", "", -1, ClusterLvl::LEVEL1);

	// Layout

	// Clear data related to the old instances of force directed layout
	imagesSource.Clear();
	forceDirectedLayouts.clear();
	drawingImagesDestination.clear();

	// Create objects that are used in force directed layout
	for (int i = 0; i < numberOfClusters; ++i) {
		ImageRepresentation imageDescriptor = classifier->GetRepresentativeImage(i);
		ClusterLvl level = ClusterLvl::LEVEL1;
		LayoutObject obj(imageDescriptor, level);
		imagesSource.Add(obj);
	}

	imagesDestination.SetCount(imagesSource.Count());

	forceDirectedLayouts.push_back(std::make_unique<vislib::math::ForceDirected<LayoutObject, float, 2> >(imagesSource,
		imagesDestination, 3000, stepLength, 3, 1, stopEnergy, damping));

	forceDirectedLayouts.back()->Init();

	// Adapt the initialisation of ForceDirected to our data
	for (int i = 0; i < imagesDestination.Count(); ++i) {
		for (int j = 0; j < 2; ++j) {
			imagesDestination[i][j] = 0.8 * ((imagesDestination[i][j] / imagesDestination.Count()) - 1.0);
		}
		drawingImagesDestination.push_back(&imagesDestination[i]);
	}

	// Compute a few steps immediately after initialization
	for (int i = 0; i < imagesSource.Count(); i++) {
		currentEnergy = forceDirectedLayouts.back()->SingleStep();
	}


	// Delete arrays that contain information related to the previous instance
	imagesSourceLvl2.clear();
	imagesDestinationsLvl2.clear();
	currentEnergysLvl2.clear();

	imagesSourceLvl2.reserve(10000);
	imagesDestinationsLvl2.reserve(10000);
	currentEnergysLvl2.reserve(10000);

	if (warningMessage)
		std::cerr << "WARNING: Number of clusters was set to a number larger "
		<< "than the number of the images. It is now adjusted to the number of images in the folder.\n";

}


/*
* ClusterRenderer::create
*/
bool megamol::msmcluster::ClusterRenderer::create(void) {

	vislib::graphics::gl::ShaderSource vert, frag;

	createLinkShaderProgram(shaderProgram, &(this->instance()->ShaderSourceFactory()), vert, frag);

	origVertices = {
		0.42f,  0.21f, 0.0f, 1.0f, 1.0f,
		0.42f,  -0.21f, 0.0f, 1.0f, 0.0f,
		-0.42f, -0.21f, 0.0f, 0.0f, 0.0f,
		-0.42f,  0.21f, 0.0f, 0.0f, 1.0f
	};
	currentVertices = origVertices;

	currentLvl2Vertices = {
		0.42f,  0.21f, 0.0f, 1.0f, 1.0f,
		0.42f,  -0.21f, 0.0f, 1.0f, 0.0f,
		-0.42f, -0.21f, 0.0f, 0.0f, 0.0f,
		-0.42f,  0.21f, 0.0f, 0.0f, 1.0f
	};
	scaleVertices(imageSizeScalling, lvl2ScaleImageSize, currentLvl2Vertices, origVertices);

	origImgRect = ImageRectangle(0.84f, 0.42f);

	currentImgRect = origImgRect;

	indices = {
		0, 1, 3,
		1, 2, 3
	};

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	glGenBuffers(1, &sVBO);
	glGenVertexArrays(1, &sVAO);

	bufferArrayObjInitialization(VBO, VAO, currentVertices, indices);
	bufferArrayObjInitialization(sVBO, sVAO, currentLvl2Vertices, indices);

	core::param::IntParam *atNumOfClustersParam = this->paramNumOfClusters.Param<core::param::IntParam>();
	numberOfClusters = atNumOfClustersParam->Value();
	clusterColors = computeColors(numberOfClusters);

	return true;
}


/*
* ClusterRenderer::drawImage
*/
void megamol::msmcluster::ClusterRenderer::drawImage(int i) {

	// Set model matrix to an identity matrix
	GLfloat model[16]{
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	// Set model to positions of the image within bbox
	model[12] = drawingImagesDestination[i]->GetX();
	model[13] = drawingImagesDestination[i]->GetY();

	shaderProgram.Enable();

	glBindTexture(GL_TEXTURE_2D_ARRAY, texArrays.at(imagesToDraw[i].texArrayIdx).id);

	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, model);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "modelView"), 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "proj"), 1, GL_FALSE, projectionMatrix);


	GLfloat borderColor[4]{ 0.0f, 0.0f, 0.0f, 1.0f };

	// when we are drawing uniform grid, border is in the same color as the border
	// of representative image of the cluster in force directed layout
	if (drawImagesInCluster.first) {
		auto parentColor = clusterColors.at(imagesToDraw[i].parentIdx);
		borderColor[0] = parentColor.GetX();
		borderColor[1] = parentColor.GetY();
		borderColor[2] = parentColor.GetZ();
	}
	// otherwise, every representative image of some cluster has a different color
	else {
		borderColor[0] = clusterColors.at(i).GetX();
		borderColor[1] = clusterColors.at(i).GetY();
		borderColor[2] = clusterColors.at(i).GetZ();
	}

	glUniform4fv(glGetUniformLocation(shaderProgram, "borderColor"), 1, borderColor);

	glUniform1i(glGetUniformLocation(shaderProgram, "texIdx"), imagesToDraw[i].inTexArrIdx);

	// Check the clustering level of the image to draw and whether grid layout is being drawn
	if (drawImagesInCluster.first || (imagesToDraw[i].level == ClusterLvl::LEVEL1)) {
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices.data());
	} else {
		glBindVertexArray(sVAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices.data());
	}

	shaderProgram.Disable();
}


/*
* ClusterRenderer::get6DofState
*/
megamol::core::view::Call6DofInteraction* megamol::msmcluster::ClusterRenderer::get6DofState(void) {
	auto c = this->slotGet6DofState.CallAs<megamol::core::view::Call6DofInteraction>();
	if ((c != nullptr) && (*c)(megamol::core::view::Call6DofInteraction::IDX_GET_STATE)) {
		return c;
	}
	else {
		return nullptr;
	}
}


/*
* ClusterRenderer::GetCapabilities
*/
bool megamol::msmcluster::ClusterRenderer::GetCapabilities(core::Call& call) {
	megamol::core::view::CallRender2D* cr = dynamic_cast<megamol::core::view::CallRender2D*>(&call);
	if (cr == NULL) return false;

	return true;
}


/*
* ClusterRenderer::GetExtents
*/
bool megamol::msmcluster::ClusterRenderer::GetExtents(megamol::core::view::CallRender2D& call) {
	
	if (bboxSizeChanged) {
		call.SetBoundingBox(bboxMinX, bboxMinY, bboxMaxX, bboxMaxY);
	} else {
		call.SetBoundingBox(-2, -1, 2, 1);
	}

	return true;
}


/*
* ClusterRenderer::loadImage
*/
GLubyte* megamol::msmcluster::ClusterRenderer::loadImage(std::string pathToImage, int idx, ImageInfo &imInfo, int &texWidth, int &texHeight, int &numTexChannels) {

	GLubyte *image = stbi_load(pathToImage.c_str(), &texWidth, &texHeight, &numTexChannels, 0);
	if (image) {
		auto predicate = [texWidth, texHeight](const TextureArray &texArr) {
			return texArr.GetWidth() == texWidth && texArr.GetHeight() == texHeight;
		};
		int pos;
		auto it = std::find_if(texArrays.begin(), texArrays.end(), predicate);
		if (it != texArrays.end()) {
			it->ImageToStore(imInfo.path, image);
			pos = it - texArrays.begin();
		} else {
			TextureArray tmpArray = TextureArray(texWidth, texHeight);
			tmpArray.ImageToStore(imInfo.path, image);
			texArrays.push_back(tmpArray);
			pos = texArrays.size() - 1;
		}
		imInfo.texArrayIdx = pos;
		imInfo.inTexArrIdx = texArrays[pos].GetIdxInStorage();

		std::cout << "Image " << idx << " " << pathToImage << " loaded" << std::endl;
	} else {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
			"Unable to load texture %s: on path %s\n", "MSMCluster", pathToImage);
	}
	return image;
}


/*
* ClusterRenderer::MouseEvent
*/
bool megamol::msmcluster::ClusterRenderer::MouseEvent(float x, float y, ::megamol::core::view::MouseFlags flags) {
	
	bboxSizeChanged = false;

	// Picking
	if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_CHANGED) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_DOWN) {
			for (int i = drawingImagesDestination.size() - 1; i > -1; --i) {
				xCorrection = x - drawingImagesDestination[i]->GetX();
				yCorrection = y - drawingImagesDestination[i]->GetY();

				// Check whether it is the image that represents a subcluster
				ImageRectangle imgRect;
				if (drawImagesInCluster.first) {
					imgRect = gridRectangle;
				} else if (imagesToDraw[i].level == ClusterLvl::LEVEL1) {
					imgRect = currentImgRect;
				} else {
					imgRect = currentImgRect * lvl2ScaleImageSize;
				}

				if (abs(xCorrection) < imgRect.width / 2) {
					if (abs(yCorrection) < imgRect.height / 2) {
						this->drag = std::make_pair(true, i);

						core::param::StringParam *atSelectedImageParam = this->paramSelectedImage.Param<core::param::StringParam>();
						atSelectedImageParam->SetValue(imagesToDraw[i].name.c_str());

						// Start grid layout
						// If CTRL is held as well, draw all of the images in this cluster
						if (flags & megamol::core::view::MOUSEFLAG_MODKEY_CTRL_DOWN) {
							drawImagesInCluster = std::make_pair(true, i);
							this->drag = std::make_pair(false, -1);
						}
						return true;
					}
				}
			}
		}
	}

	// Dragging
	if (this->drag.first) {
		drawingImagesDestination[this->drag.second]->SetX(x - xCorrection);
		drawingImagesDestination[this->drag.second]->SetY(y - yCorrection);
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_CHANGED) {
			this->drag = std::make_pair(false, -1);
		}
	}


	// Subclustering
	if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_CHANGED) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_DOWN) {
			if (!drawImagesInCluster.first)
				for (int i = 0; i < imagesDestination.Count(); ++i) {
					xCorrection = x - imagesDestination[i][0];
					yCorrection = y - imagesDestination[i][1];
					if (abs(xCorrection) < currentImgRect.width / 2) {
						if (abs(yCorrection) < currentImgRect.height / 2) {
							clusterToSubcluster = std::make_pair(true, i);
							return true;
						}
					}
				}
		}
	}


	//Zooming

	// Zoom in
	if (flags & megamol::core::view::MOUSEFLAG_MODKEY_CTRL_DOWN) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_CHANGED) {
			if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_DOWN) {

				if (diffZoomInOut == 0) {
					bboxMinX += bboxMinX * zoomingSensitivity;
					bboxMinY += bboxMinY * zoomingSensitivity;
					bboxMaxX += bboxMaxX * zoomingSensitivity;
					bboxMaxY += bboxMaxY * zoomingSensitivity;
				}

				bboxSizeChanged = true;
				return true;
			}
		}
	}
	// Zoom out
	if (flags & megamol::core::view::MOUSEFLAG_MODKEY_SHIFT_DOWN) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_CHANGED) {
			if (flags & megamol::core::view::MOUSEFLAG_BUTTON_RIGHT_DOWN) {

				if (diffZoomInOut == 0) {
					bboxMinX -= bboxMinX * zoomingSensitivity;
					bboxMinY -= bboxMinY * zoomingSensitivity;
					bboxMaxX -= bboxMaxX * zoomingSensitivity;
					bboxMaxY -= bboxMaxY * zoomingSensitivity;
				}

				bboxSizeChanged = true;
				return true;
			}
		}
	}

	return false;

}


/*
* ClusterRenderer::onParamBackPressed
*/
bool megamol::msmcluster::ClusterRenderer::onParamBackPressed(megamol::core::param::ParamSlot &paramSlot) {
	// do nothing if we are not in grid layout
	if (!drawImagesInCluster.first)
		return true;

	// Because pointers in drawingImagesDestination were in grid layout allocated
	// on heap, delete them
	for (auto* dest : drawingImagesDestination)
		delete dest;


	drawingImagesDestination = storedImagesDestination;
	imagesToDraw = storedImagesToDraw;
	origVertices = storedOrigVertices;
	for (auto &texArray : texArrays) {
		texArray.ResetStorage(texImgCacheNames);
	}

	drawImagesInCluster = std::make_pair(false, -1);
	gridDrawn = false;

	core::param::StringParam *atParamClusterName = this->paramClusterName.Param<core::param::StringParam>();
	atParamClusterName->SetValue("not in uniform grid");

	// load earlier images
	for (int i = 0; i < imagesToDraw.size(); ++i) {
		// if image is in cache it doesn't have to be loaded, existing one is added to corresponding texture array
		if (!checkLoadTexCache(imagesToDraw[i], i)) {
			int texWidth, texHeight, numTexChannels;
			GLubyte *image = loadImage(imagesToDraw[i].path, i, imagesToDraw[i], texWidth, texHeight, numTexChannels);
		}
	}
	// Activate texture storage and generate mipmap
	for (auto &texArray : texArrays) {
		texArray.StoreImages(lvl2NumOfClusters + 5);
		texArray.GenMipmap();
	}

	// reset the size
	bufferArrayObjInitialization(VBO, VAO, currentVertices, indices);

	// debug - temporarily divide x by 2
	x = x / 2;
	if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_CHANGED) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_DOWN) {
			for (int i = 0; i < imagesDestination.Count(); ++i) {
				xCorrection = x - imagesDestination[i][0];
				yCorrection = y - imagesDestination[i][1];
				if (abs(xCorrection) < 0.3) {
					if (abs(yCorrection) < 0.15) {
						this->drag = std::make_pair(true, i);
						currentEnergy = forceDirected.SingleStep();
						return true;
					}
				}
			}
		}
	}

	if (this->drag.first) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_CHANGED) {
			this->drag = std::make_pair(false, -1);
			return false;
		}
		imagesDestination[this->drag.second][0] = x - xCorrection;
		imagesDestination[this->drag.second][1] = y - yCorrection;
	}

	// debug - if it returns false, the bounding box appears and is responsive to picking
	return false;
}


/*
* ClusterRenderer::onParamClusterPressed
*/
bool megamol::msmcluster::ClusterRenderer::onParamClusterPressed(megamol::core::param::ParamSlot &paramSlot) {
	// If trying to cluster but still in grid layout
	if (drawImagesInCluster.first)
		onParamBackPressed(this->paramBack);

	// Check whether the user changed number of clusters
	if (this->paramNumOfClusters.IsDirty()) {
		core::param::IntParam *atNumOfClustersParam = this->paramNumOfClusters.Param<core::param::IntParam>();
		numberOfClusters = atNumOfClustersParam->Value();
		clusterColors = computeColors(numberOfClusters);
		paramImageSizeScalling.ResetDirty();
	}

	// Empty string because this initial clustering doesn't have a parent
	clusterAndInitializeTextureArray();
	return true;
}


/*
* ClusterRenderer::onParamZoomInPressed
*/
bool megamol::msmcluster::ClusterRenderer::onParamZoomInPressed(megamol::core::param::ParamSlot &paramSlot) {
	if (diffZoomInOut == 0) {
		bboxMinX += bboxMinX * zoomingSensitivity;
		bboxMinY += bboxMinY * zoomingSensitivity;
		bboxMaxX += bboxMaxX * zoomingSensitivity;
		bboxMaxY += bboxMaxY * zoomingSensitivity;
	}
	bboxSizeChanged = true;

	std::cerr << "In ZoomIn";
	return true;
}


/*
* ClusterRenderer::onParamZoomOutPressed
*/
bool megamol::msmcluster::ClusterRenderer::onParamZoomOutPressed(megamol::core::param::ParamSlot &paramSlot) {
	if (diffZoomInOut == 0) {
		bboxMinX -= bboxMinX * zoomingSensitivity;
		bboxMinY -= bboxMinY * zoomingSensitivity;
		bboxMaxX -= bboxMaxX * zoomingSensitivity;
		bboxMaxY -= bboxMaxY * zoomingSensitivity;
	}
	bboxSizeChanged = true;
	return true;
}


bool megamol::msmcluster::ClusterRenderer::OnGetCamParams(megamol::core::view::CallCamParamSync& c){
	c.SetCamParams(this->cam.Parameters());
	return true;
}

/*
* ClusterRenderer::release
*/
void megamol::msmcluster::ClusterRenderer::release(void) {
	shaderProgram.Release();
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &sVAO);
	glDeleteBuffers(1, &sVBO);
	for (TexCache &cacheEntry : texImgCache) {
		stbi_image_free(cacheEntry.image);
	}
	for (auto &texArray : texArrays) {
		glDeleteTextures(1, &(texArray.id));
	}
}


/*
* ClusterRenderer::Render
*/
bool megamol::msmcluster::ClusterRenderer::Render(megamol::core::view::CallRender2D& call) {
	// Button mask for selecting.
	auto selectionButtons = static_cast<
		megamol::core::view::Call6DofInteraction::ButtonMaskType>(
			this->paramSelectionButtons.Param<core::param::IntParam>()->Value());

	this->SyncCamParams(this->camParams);
#if (defined(DEBUG) || defined(_DEBUG))
	auto screenSize = this->camParams->VirtualViewSize();
	
#endif
	auto callTrackerState = this->get6DofState();
	if ((callTrackerState != nullptr) && callTrackerState->IsValid()) {
		this->xScale = this->paramXScale.Param<core::param::FloatParam>()->Value();
		this->yScale = this->paramYScale.Param<core::param::FloatParam>()->Value();
		this->zScale = this->paramZScale.Param<core::param::FloatParam>()->Value();


		auto hit = this->get6DofIntersection(callTrackerState);
		auto valid_hit = (hit.GetX() != -FLT_MAX && hit.GetY() != -FLT_MAX); // valid-hit bei treffen von Cluster
		if (valid_hit) {
			printf("x: %f y: %f\n", hit.GetX(), hit.GetY());
		}

		auto trackingPosition = callTrackerState->GetPosition();   // Position des Trackingsticks 
		
		auto trackingOrientation = callTrackerState->GetOrientation(); // Rotation des Trackingsticks

		auto trackingButtonStates = callTrackerState->GetButtonStates();
		
		auto diffPos = oldPos - trackingPosition;

		float y = hit.GetY();// *yScale;
		float x = hit.GetX();// *xScale;
		float newX = (4.0f * x) - 2.0f;
		float newY = (2.0f * y) - 1.0f;

	 /*
	 0x00 == Kein Button gedr�ckt					 --> Nichts machen
	 0x01 == vorne (auf seite von Schrift)			 --> Drap and drop (valid_hit) && UNDO (!valid_hit)
	 0x02 == hinten	&& richtung von Powerwall laufen --> Selection
	 0x02 == hinten && weg von Powerwall laufen		 --> Sublcustering
	 0x03 == beide									 --> Zoomen
	 */

		if (trackingButtonStates == 0x00) {
			/* Do nothing*/
		}
		else if (trackingButtonStates == 0x01) {
			/* Drag and Drop*/
			if (valid_hit) {
				for (int i = drawingImagesDestination.size() - 1; i > -1; --i) {
					xCorrection =  newX - drawingImagesDestination[i]->GetX();
					yCorrection =  newY - drawingImagesDestination[i]->GetY();
					
					// Check whether it is the image that represents a subcluster
					ImageRectangle imgRect;
					if (drawImagesInCluster.first) {
						imgRect = gridRectangle;
					}
					else if (imagesToDraw[i].level == ClusterLvl::LEVEL1) {
						imgRect = currentImgRect;
					}
					else {
						imgRect = currentImgRect * lvl2ScaleImageSize;
					}
					if (abs(xCorrection) < imgRect.width / 2) {
						if (abs(yCorrection) < imgRect.height / 2) {
							this->drag = std::make_pair(true, i);
							// currentEnergy = forceDirectedLayouts[0]->SingleStep();
							core::param::StringParam *atSelectedImageParam = this->paramSelectedImage.Param<core::param::StringParam>();
							atSelectedImageParam->SetValue(imagesToDraw[i].name.c_str());
							return true;
						}
					}
				}
				if (this->drag.first) {
					(*drawingImagesDestination[this->drag.second])[0] = newX - xCorrection ;
					(*drawingImagesDestination[this->drag.second])[1] = newY - yCorrection ;
				}
	
			}
			/* Undo the Selection*/
			else {
				// do nothing if we are not in grid layout
				if (!drawImagesInCluster.first)
					return true;

				// Because pointers in drawingImagesDestination were in grid layout allocated
				// on heap, delete them
				for (auto* dest : drawingImagesDestination)
					delete dest;

				drawingImagesDestination = storedImagesDestination;
				imagesToDraw = storedImagesToDraw;
				origVertices = storedOrigVertices;
				for (auto &texArray : texArrays) {
					texArray.ResetStorage(texImgCacheNames);
				}

				drawImagesInCluster = std::make_pair(false, -1);
				gridDrawn = false;

				core::param::StringParam *atParamClusterName = this->paramClusterName.Param<core::param::StringParam>();
				atParamClusterName->SetValue("not in uniform grid");

				// load earlier images
				for (int i = 0; i < imagesToDraw.size(); ++i) {
					// if image is in cache it doesn't have to be loaded, existing one is added to corresponding texture array
					if (!checkLoadTexCache(imagesToDraw[i], i)) {
						int texWidth, texHeight, numTexChannels;
						GLubyte *image = loadImage(imagesToDraw[i].path, i, imagesToDraw[i], texWidth, texHeight, numTexChannels);
					}
				}
				// Activate texture storage and generate mipmap
				for (auto &texArray : texArrays) {
					texArray.StoreImages(lvl2NumOfClusters + 5);
					texArray.GenMipmap();
				}

				// reset the size
				bufferArrayObjInitialization(VBO, VAO, currentVertices, indices);
				return true;
			}

		}
		/* Stop draging*/
		else if (oldTrackingButtonStates == 0x01 && trackingButtonStates == 0x00) {
			this->drag = std::make_pair(false, -1);
		}
		
		else if (trackingButtonStates == 0x02) {
			if (valid_hit) {
				/* Selection*/
				if (diffPos.GetZ() > 0.0f) {
					for (int i = drawingImagesDestination.size() - 1; i > -1; --i) {
						xCorrection = newX - drawingImagesDestination[i]->GetX();
						yCorrection = newY - drawingImagesDestination[i]->GetY();
						// Check whether it is the image that represents a subcluster
						ImageRectangle imgRect;
						if (drawImagesInCluster.first) {
							imgRect = gridRectangle;
						}
						else if (imagesToDraw[i].level == ClusterLvl::LEVEL1) {
							imgRect = currentImgRect;
						}
						else {
							imgRect = currentImgRect * lvl2ScaleImageSize;
						}
						if (abs(xCorrection) < imgRect.width / 2) {
							if (abs(yCorrection) < imgRect.height / 2) {
								this->drag = std::make_pair(true, i);
								
								core::param::StringParam *atSelectedImageParam = this->paramSelectedImage.Param<core::param::StringParam>();
								atSelectedImageParam->SetValue(imagesToDraw[i].name.c_str());

								// Start grid layout
								//  draw all of the images in this cluster
											
								drawImagesInCluster = std::make_pair(true, i);
								this->drag = std::make_pair(false, -1);
													
								return true;
							}
						}
					}
					if (this->drag.first) {
						(*drawingImagesDestination[this->drag.second])[0] = newX - xCorrection;
						(*drawingImagesDestination[this->drag.second])[1] = newY - yCorrection;
					}
				}
				/* Subclustering*/
				else if (diffPos.GetZ() < 0) {
					for (int i = 0; i < imagesDestination.Count(); ++i) {
						xCorrection = newX - imagesDestination[i][0];
						yCorrection = newY - imagesDestination[i][1];
						if (abs(xCorrection) < currentImgRect.width / 2) {
							if (abs(yCorrection) < currentImgRect.height / 2) {
								clusterToSubcluster = std::make_pair(true, i);
								return true;
							}
						}
					}
				}
			}
		}
		/* Zooming*/
		else if (trackingButtonStates == 0x03) {
			/* Zoom In*/
			if (diffPos.GetZ() > 0.0f) {
				bboxMinX -= bboxMinX * zScale;
				bboxMinY -= bboxMinY * zScale;
				bboxMaxX -= bboxMaxX * zScale;
				bboxMaxY -= bboxMaxY * zScale;
				bboxSizeChanged = true;
				return true;
			}
			/* Zoom Out*/
			else if (diffPos.GetZ() < 0.0f)
			{
				bboxMinX += bboxMinX * zScale;
				bboxMinY += bboxMinY * zScale;
				bboxMaxX += bboxMaxX * zScale;
				bboxMaxY += bboxMaxY * zScale;
				bboxSizeChanged = true;
				return true;
			}		
		}

		this->oldTrackingButtonStates = trackingButtonStates;
		this->oldPos = trackingPosition;
	}


	megamol::core::view::CallRender2D* cr = dynamic_cast<megamol::core::view::CallRender2D*>(&call);
	if (cr == NULL) return false;

	if (bboxSizeChanged) {

		auto resetView = this->FindNamedObject("::inst::View2D1")
			->GetCoreInstance()
			->FindParameter("::inst::View2D1::resetView");

		resetView->setDirty();
		toMoveX += previousModelViewX;
		toMoveY += previousModelViewY;
		bboxSizeChanged = false;
	}


	glGetFloatv(GL_MODELVIEW_MATRIX, modelViewMatrix);
	glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrix);


	// Check whether the parameters were changed

	if (paramLvl2NumOfClusters.IsDirty()) {
		core::param::IntParam *atParamLvl2NumOfClusters = this->paramLvl2NumOfClusters.Param<core::param::IntParam>();
		lvl2NumOfClusters = atParamLvl2NumOfClusters->Value();
		paramLvl2NumOfClusters.ResetDirty();
	}

	if (paramZoomingSensitivity.IsDirty()) {
		core::param::FloatParam *atParamZoomingSensitivity = this->paramZoomingSensitivity.Param<core::param::FloatParam>();
		zoomingSensitivity = atParamZoomingSensitivity->Value();
		paramZoomingSensitivity.ResetDirty();
	}

	if (paramImageSizeScalling.IsDirty()) {

		core::param::FloatParam *atImageSizeParam = this->paramImageSizeScalling.Param<core::param::FloatParam>();
		imageSizeScalling = atImageSizeParam->Value();
		currentImgRect = {
			imageSizeScalling * origImgRect.width,
			imageSizeScalling * origImgRect.height
		};

		scaleVertices(imageSizeScalling, 1, currentVertices, origVertices);
		scaleVertices(imageSizeScalling, lvl2ScaleImageSize, currentLvl2Vertices, origVertices);

		bufferArrayObjInitialization(VBO, VAO, currentVertices, indices);
		bufferArrayObjInitialization(sVBO, sVAO, currentLvl2Vertices, indices);

		paramImageSizeScalling.ResetDirty();
	}

	if (paramLvl2ScaleImageSize.IsDirty()) {

		core::param::FloatParam *atParamLvl2ScaleImageSize = this->paramLvl2ScaleImageSize.Param<core::param::FloatParam>();
		lvl2ScaleImageSize = atParamLvl2ScaleImageSize->Value();

		scaleVertices(imageSizeScalling, lvl2ScaleImageSize, currentLvl2Vertices, origVertices);

		bufferArrayObjInitialization(sVBO, sVAO, currentLvl2Vertices, indices);

		paramLvl2ScaleImageSize.ResetDirty();
	}

	if (paramStopEnergy.IsDirty()) {
		core::param::FloatParam *atParamStopEnergy = this->paramStopEnergy.Param<core::param::FloatParam>();
		// Division because there are only three decimal places in parameter slot
		stopEnergy = atParamStopEnergy->Value() / 1000000;
		paramImageSizeScalling.ResetDirty();
	}

	if (paramDamping.IsDirty()) {
		core::param::FloatParam *atParamDamping = this->paramDamping.Param<core::param::FloatParam>();
		damping = atParamDamping->Value();
		paramImageSizeScalling.ResetDirty();
	}

	if (paramStepLength.IsDirty()) {
		core::param::FloatParam *atParamStepLength = this->paramStepLength.Param<core::param::FloatParam>();
		damping = atParamStepLength->Value();
		paramImageSizeScalling.ResetDirty();
	}

	if (this->paramTexImgCacheSizeLimit.IsDirty()) {
		core::param::IntParam *atParamTexImgCacheSizeLimit = this->paramTexImgCacheSizeLimit.Param<core::param::IntParam>();
		texImgCacheSizeLimit = atParamTexImgCacheSizeLimit->Value();
		paramTexImgCacheSizeLimit.ResetDirty();
	}

	if (clusterToSubcluster.first) {
		clusterToSubcluster.first = false;
		this->subcluster(imagesToDraw[clusterToSubcluster.second].name, imagesToDraw[clusterToSubcluster.second].path, clusterToSubcluster.second);
	}

	// Check whether the user wants to draw all of the images in some cluster
	if (drawImagesInCluster.first) {

		if (!gridDrawn) {

			// write in a parameter name of the representative image of this cluster
			core::param::StringParam *atParamClusterName = this->paramClusterName.Param<core::param::StringParam>();
			atParamClusterName->SetValue(imagesToDraw[drawImagesInCluster.second].name.c_str());

			ImageInfo represClusterImage = imagesToDraw[drawImagesInCluster.second];
			std::vector<ImageRepresentation> cluster = clustersList[std::make_pair(represClusterImage.path, represClusterImage.level)];

			// Save the current state
			storedImagesDestination = drawingImagesDestination;
			storedImagesToDraw = imagesToDraw;
			storedOrigVertices = origVertices;

			drawingImagesDestination.clear();
			imagesToDraw.clear();
			for (auto &texArray : texArrays) {
				texArray.ResetStorage(texImgCacheNames);
			}

			// In ImageInfo only needed pieces of info are filled
			for (int i = 0; i < cluster.size(); ++i) {
				ImageInfo imInfo(
					pathToName(cluster[i].path),
					cluster[i].path,
					i,
					represClusterImage.name,
					represClusterImage.path,
					represClusterImage.idx,
					ClusterLvl::LEVEL1,
					false
				);
				imagesToDraw.push_back(imInfo);
			}

			std::cerr << "Size of the imagesToDraw: " << imagesToDraw.size() << "\n";

			// Set origVertices to the size of the bbox
			origVertices = {
				2.0f,  1.0f, 0.0f, 1.0f, 1.0f,
				2.0f,  -1.0f, 0.0f, 1.0f, 0.0f,
				-2.0f, -1.0f, 0.0f, 0.0f, 0.0f,
				-2.0f,  1.0f, 0.0f, 0.0f, 1.0f
			};
			std::vector<GLfloat> currentVertices = origVertices;

			// Positioning in the grid layout
			int rowsColumns = ceil(sqrt(cluster.size()));
			float stepX = (static_cast<float>(bboxMaxX - bboxMinX) / rowsColumns) / 2;
			float stepY = (static_cast<float>(bboxMaxY - bboxMinY) / rowsColumns) / 2;

			// second scaling factor of 0.9, so that the images don't touch
			scaleVertices(1.0f / rowsColumns, 0.9, currentVertices, origVertices);
			gridRectangle = ImageRectangle(4.0f, 2.0f) * (1.0f / rowsColumns) * 0.9;
			bufferArrayObjInitialization(VBO, VAO, currentVertices, indices);
			std::cerr << "stepX: " << stepX << "\n";
			std::cerr << "stepY: " << stepY << "\n";
			// Draw images from the bottom right corner
			float x = 2.0 - stepX;
			int count = 0;
			for (int i = 0; i < rowsColumns; ++i) {
				float y = -1.0 + stepY;
				for (int j = 0; j < rowsColumns; ++j) {
					std::cerr << "count: " << count << "<" << x << ", " << y << ">" << "\n";
					drawingImagesDestination.push_back(new vislib::math::Vector<float, 2>(x, y));
					int texWidth, texHeight, numTexChannels;
					loadImage(imagesToDraw[count].path, count, imagesToDraw[count], texWidth, texHeight, numTexChannels);
					++count;
					std::cerr << "count: " << count << "<" << x << ", " << y << ">" << "\n";
					y += 2.0 * stepY;
					std::cerr << (count >= imagesToDraw.size()) << "\n";
					if (count >= imagesToDraw.size()) {
						// break the outer loop
						i = rowsColumns;
						j = rowsColumns;
					}
				}
				x -= 2.0 * stepX;
			}
			// Activate texture storage and generate mipmap
			for (auto &texArray : texArrays) {
				texArray.StoreImages(lvl2NumOfClusters + 5);
				texArray.GenMipmap();
			}
			gridDrawn = true;
		}

		// Draw images
		for (int i = 0; i < drawingImagesDestination.size(); ++i) {
			drawImage(i);
		}

		// Continue the rendering loop
		return true;
	}


	// Layout calculation

	for (int i = 1; i < forceDirectedLayouts.size(); ++i) {
		// In forceDirectedLayouts first level 2 layout is stored on index 1 
		// whereas in currentEnergysLvl2 its corresponding current energy is
		// stored on index 0
		if (currentEnergysLvl2[i - 1] > stopEnergyLvl2 || this->drag.first) {
			currentEnergysLvl2[i - 1] = forceDirectedLayouts.at(i)->SingleStep();
		}
	}


	if (forceDirectedLayouts.size() > 0 && (currentEnergy > stopEnergy || this->drag.first)) {
		// We have to multiply it because the parameter slot shows only three decimal places.
		this->paramCurrentEnergy.Param<core::param::FloatParam>()->SetValue(currentEnergy * 1000000);
		if (forceDirectedLayouts.at(0) != nullptr)
			currentEnergy = forceDirectedLayouts.at(0)->SingleStep();
	}


	// Check the bounds
	ImageRectangle imgRect;
	for (int i = 0; i < imagesDestination.Count(); ++i) {

		imgRect = currentImgRect;

		if (imagesDestination[i][0] > 1.9 - imgRect.width / 2)
			imagesDestination[i][0] = 1.9 - imgRect.width / 2;
		else if (imagesDestination[i][0] < -1.9 + imgRect.width / 2)
			imagesDestination[i][0] = -1.9 + imgRect.width / 2;
		if (imagesDestination[i][1] > 0.9 - imgRect.height / 2)
			imagesDestination[i][1] = 0.9 - imgRect.height / 2;
		else if (imagesDestination[i][1] < -0.9 + imgRect.height / 2)
			imagesDestination[i][1] = -0.9 + imgRect.height / 2;


		drawingImagesDestination[i] = &imagesDestination[i];

	}

	int countDrawing = imagesDestination.Count();
	for (auto &currentImagesDestination : imagesDestinationsLvl2) {

		for (int i = 0; i < currentImagesDestination.Count() - 1; ++i) {

			imgRect = currentImgRect * lvl2ScaleImageSize;

			if (currentImagesDestination[i][0] > 1.9 - imgRect.width / 2)
				currentImagesDestination[i][0] = 1.9 - imgRect.width / 2;
			else if (currentImagesDestination[i][0] < -1.9 + imgRect.width / 2)
				currentImagesDestination[i][0] = -1.9 + imgRect.width / 2;
			if (currentImagesDestination[i][1] > 0.9 - imgRect.height / 2)
				currentImagesDestination[i][1] = 0.9 - imgRect.height / 2;
			else if (currentImagesDestination[i][1] < -0.9 + imgRect.height / 2)
				currentImagesDestination[i][1] = -0.9 + imgRect.height / 2;


			drawingImagesDestination[countDrawing] = &currentImagesDestination[i];
			++countDrawing;
		}

		int parentIdx = imagesToDraw[countDrawing - 1].parentIdx;

		// Take parent image into account when calculating the layout as well
		currentImagesDestination[currentImagesDestination.Count() - 1] = imagesDestination[parentIdx];
	}

	for (GLuint i = 0; i < imagesToDraw.size(); ++i) {

		// if this is the selected image, it should be drawn last so that it is in 
		// the foreground
		if (this->drag.first && i == this->drag.second) {
			continue;
		}

		drawImage(i);
	}

	// Draw the selected image in the foreground
	if (this->drag.first)
		drawImage(this->drag.second);



	return true;
}


/*
* ClusterRenderer::setUpTextures
*/
void megamol::msmcluster::ClusterRenderer::setUpTextures(int numOfClusters, std::string parentName, 
	std::string parentPath, int parentIdx, ClusterLvl level) 
{

	int numOfImagesToDrawBefore = imagesToDraw.size();
	// Texture array initialization
	for (int i = 0; i < numOfClusters; ++i) {
		// 0.0 coordinate on the y-axis should in openGL be on the bottom of the image
		stbi_set_flip_vertically_on_load(true);

		std::string representativeImage = classifier->GetRepresentativeImage(i).path;

		std::string fileName = pathToName(representativeImage);

		ImageInfo imInfo(fileName, representativeImage, numOfImagesToDrawBefore + i,
			parentName, parentPath, parentIdx, level, false);

		if (!checkLoadTexCache(imInfo, numOfImagesToDrawBefore + i)) {
			int texWidth, texHeight, numTexChannels;

			GLubyte* image = loadImage(representativeImage, i, imInfo, texWidth, texHeight, numTexChannels);

			if (texImgCache.size() > texImgCacheSizeLimit) {
				stbi_image_free(texImgCache.front().image);
				texImgCache.erase(texImgCache.begin());
				texImgCacheNames.erase(texImgCacheNames.begin());
			}
			texImgCache.push_back(
				TexCache(image, texWidth, texHeight, numTexChannels, imInfo.texArrayIdx, imInfo.inTexArrIdx)
			);
			texImgCacheNames.push_back(representativeImage);

			// debug
			GLenum errorCode;
			while ((errorCode = glGetError()) != GL_NO_ERROR) {
				std::string error;
				switch (errorCode) {
				case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
				case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
				case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
				case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
				case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
				case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
				case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
				}
				std::cout << error << std::endl;
			}
		}
		// In both cases we add an image to those that should be drawn
		imagesToDraw.push_back(imInfo);

		if (level == ClusterLvl::LEVEL2) {
			// Set border color to the color of the parent
			clusterColors.push_back(clusterColors[parentIdx]);
		}

	}

	// Activate texture storage and generate mipmap
	for (auto &texArray : texArrays) {
		// Create array if clustering on level 1
		texArray.StoreImages(lvl2NumOfClusters + 5, level == ClusterLvl::LEVEL1);
		texArray.GenMipmap();
	}
}


/*
* ClusterRenderer::subcluster
*/
void megamol::msmcluster::ClusterRenderer::subcluster(std::string parent, std::string parentPath, int parentIdx) {

	// Check whether it was already done for this cluster
	if (imagesToDraw[parentIdx].subclustered)
		return;

	int numOfImagesToDrawBefore = imagesToDraw.size();
	int currentLvl2NumOfClusters = lvl2NumOfClusters;

	std::pair<std::string, ClusterLvl> parentIdentifer = std::make_pair(parentPath, ClusterLvl::LEVEL1);
	// Handle the case where the numOfClusters > number of images
	if (currentLvl2NumOfClusters > clustersList[parentIdentifer].size()) {
		// Corresponding parameter is left as it is because other clusters might have more 
		currentLvl2NumOfClusters = clustersList[parentIdentifer].size();

		warningMessage = true;

		// Handle the case where there is only one element in the cluster
		if (currentLvl2NumOfClusters <= 1) {
			std::cerr << "WARNING: There is only one element in the cluster. No elements to cluster.\n";
			return;
		}
	}



	// Clustering

	// Check which algo is chosen
	auto clusterAlgo = this->paramClusteringAlgo.Param<core::param::EnumParam>()->Value();

	switch (clusterAlgo) {
	case ClusterAlgo::KMEANS:
		classifier.reset(new KMeans(clustersList[parentIdentifer], currentLvl2NumOfClusters));
		break;
	case ClusterAlgo::DLIB_KMEANS:
		classifier.reset(new Dlib_kMeans(clustersList[parentIdentifer], currentLvl2NumOfClusters));
		break;
	case ClusterAlgo::BOTTOM_UP_AGGLOMERATIVE:
		classifier.reset(new BottomUpAgglomerative(clustersList[parentIdentifer], currentLvl2NumOfClusters));
		break;
	default:
		// temporarily
		classifier.reset(new KMeans(clustersList[parentIdentifer], currentLvl2NumOfClusters));
	}

	classifier->Run();

	for (int i = 0; i < classifier->GetClusters().size(); ++i) {
		clustersList[std::make_pair(classifier->GetRepresentativeImage(i).path, ClusterLvl::LEVEL2)] = classifier->GetCluster(i);
	}
	for (int i = 0; i < classifier->GetNumOfClusters(); ++i) {
		std::cout << "Representative image of " << i + 1 << ": " << classifier->GetRepresentativeImage(i).path << std::endl;
	}


	setUpTextures(currentLvl2NumOfClusters, parent, parentPath, parentIdx, ClusterLvl::LEVEL2);


	// Setting up the layout
	vislib::Array<LayoutObject> currentImagesSource;
	vislib::Array<vislib::math::Vector<float, 2> > currentImagesDestination;
	for (int i = 0; i < currentLvl2NumOfClusters; ++i) {

		ImageRepresentation imageProperties = classifier->GetRepresentativeImage(i);
		ClusterLvl level = ClusterLvl::LEVEL2;
		LayoutObject obj(imageProperties, level);
		currentImagesSource.Add(obj);
		// float toAdd = 0.8 * ((imagesDestination[i][] / imagesDestination.Count()) - 1.0);
		// imagesDestination.Add(vislib::math::Vector<float, 2>());
	}

	// Add parent image
	currentImagesSource.Add(imagesSource[parentIdx]);

	currentImagesDestination.SetCount(currentImagesSource.Count());

	imagesSourceLvl2.push_back(currentImagesSource);
	imagesDestinationsLvl2.push_back(currentImagesDestination);


	activeforceDirected = std::make_unique<vislib::math::ForceDirected<LayoutObject, float, 2> >(imagesSourceLvl2.back(),
		imagesDestinationsLvl2.back(), 3000, stepLength, 3, 1, stopEnergy, damping);

	activeforceDirected->Init();

	// Adapt the initialisation of ForceDirected to our data (- 1 because we don't initialize the parent positions)
	for (int i = 0; i < currentImagesDestination.Count() - 1; ++i) {
		//for (int j = 0; j < 2; ++j) {
		//	// currentImagesDestination[i][j] = 0.8 * ((currentImagesDestination[i][j] / currentImagesDestination.Count()) - 1.0);
		//}
		currentImagesDestination[i] = imagesDestination[parentIdx];
		drawingImagesDestination.push_back(&currentImagesDestination[i]);
	}
	currentImagesDestination[currentLvl2NumOfClusters - 1] = imagesDestination[parentIdx];

	// Initialize currentEnergy
	currentEnergysLvl2.push_back(activeforceDirected->SingleStep());

	forceDirectedLayouts.push_back(std::move(activeforceDirected));

	std::cout << currentImagesSource.Count() << std::endl;
	std::cout << currentImagesDestination.Count() << std::endl;
	std::cout << imagesToDraw.size() << std::endl;

	imagesToDraw[parentIdx].subclustered = true;

	std::cout << "numOfImagesToDrawBefore" << numOfImagesToDrawBefore << std::endl;

	if (warningMessage)
		std::cerr << "WARNING: Number of subclusters was set to a number larger "
		<< "than the number of the images in this cluster. All of the images in the cluster are shown.\n";

}


/*
* megamol::ClusterRenderer::get6DofIntersection
*/
vislib::graphics::SceneSpacePoint2D
megamol::msmcluster::ClusterRenderer::get6DofIntersection(
	const core::view::Call6DofInteraction *c) {
	using namespace megamol::core;
	using namespace vislib::graphics;

	vislib::graphics::SceneSpacePoint2D retval(-FLT_MAX, -FLT_MAX);

	auto h = static_cast<SceneSpaceType>(
		this->paramPhysicalHeight.Param<param::FloatParam>()->Value());
	auto w = static_cast<SceneSpaceType>(
		this->paramPhysicalWidth.Param<param::FloatParam>()->Value());
	auto o = this->paramPhysicalOrigin.Param<param::Vector3fParam>()->Value();

	if ((c != nullptr) && (h > 0) && (w > 0)) {
		auto vvs = this->cam.Parameters()->VirtualViewSize();
		auto orientation = c->GetOrientation();
		auto position = c->GetPosition();
		//VLTRACE(vislib::Trace::LEVEL_INFO, "Tracker at (%f, %f, %f), orientation "
		//	"(%f, %f, %f; %f).\n", position.X(), position.Y(), position.Z(),
		//	orientation.X(), orientation.Y(), orientation.Z(), orientation.W());

		auto dir = this->paramPhysicalPointerIdle.Param<param::Vector3fParam>(
			)->Value();
		dir = orientation * dir;
		dir.Normalise();
		//VLTRACE(vislib::Trace::LEVEL_INFO, "Pointing direction (%f, %f, %f).\n",
		//	dir.X(), dir.Y(), dir.Z());

		// TODO: This assumes screen lies on xy plane, which is currently the case. 
		// We need more user input to change this.
		auto v1 = SceneSpaceVector3D(1, 0, 0);
		auto v2 = SceneSpaceVector3D(0, 1, 0);
		auto sn = v1.Cross(v2);
		//VLTRACE(vislib::Trace::LEVEL_INFO, "Screen from (%f, %f, %f) to "
		//	"(%f, %f, %f).\n", o.X(), o.Y(), o.Z(), o.X() + w, o.Y() + h, o.Z());
		//VLTRACE(vislib::Trace::LEVEL_INFO, "Screen normal (%f, %f, %f).\n",
		//	sn.X(), sn.Y(), sn.Z());

		// Ensure that the screen and the pointing direction not parallel.
		if (sn.Dot(dir) != 0) {
			auto s2d = position - SceneSpacePoint3D(o.X(), o.Y(), o.Z());
			auto lambda = -sn.Dot(s2d) / sn.Dot(dir);
			auto i = position + lambda * dir;
			//VLTRACE(vislib::Trace::LEVEL_INFO, "Intersects at (%f, %f, %f).\n",
			//	i.X(), i.Y(), i.Z());

			// Ensure that intersection lies on physical screen.
			if ((i.X() >= o.X()) && (i.Y() >= o.Y())
				&& (i.X() <= o.X() + w) && (i.Y() <= o.Y() + h)) {
				//VLTRACE(vislib::Trace::LEVEL_INFO, "Intersection in screen.\n") ;
				float x = (i.X() - o.X()) / w;
				float y = (i.Y() - o.Y()) / h;
				//VLTRACE(vislib::Trace::LEVEL_INFO, "Relative intersection at "
				//	"(%f, %f).\n", x, y);
				retval.Set(x, y);
			}
		}
	}

	return retval;
}