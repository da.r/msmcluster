#include "stdafx.h"

#include "vislib/graphics/gl/IncludeAllGL.h"

#include "stb_image.h"
#include "TextureArray.h"


/**
* TextureArray::TextureArray
*/
megamol::msmcluster::TextureArray::TextureArray(int width, int height) 
	: width{ width }, height{ height } { }


/**
* TextureArray::createTextureArray
*/
void megamol::msmcluster::TextureArray::createTextureArray() {
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D_ARRAY, id);

	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 5, GL_RGB8, width, height, size);

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}


/**
* TextureArray::GenMipmap
*/
void megamol::msmcluster::TextureArray::GenMipmap() {
	glBindTexture(GL_TEXTURE_2D_ARRAY, id);
	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
}

/**
* TextureArray::ImageToStore
*/
void megamol::msmcluster::TextureArray::ImageToStore(std::string name, GLubyte *image) {
	imagesToStore.push_back(std::make_pair(name, image));
}


/**
* TextureArray::ResetStorage
*/
void megamol::msmcluster::TextureArray::ResetStorage(std::vector<std::string> &texImgCacheNames) {
	for (std::pair<std::string, GLubyte *> &storedImage : imagesToStore) {
		auto cacheNamesIter = std::find(texImgCacheNames.begin(), texImgCacheNames.end(), storedImage.first);
		if (cacheNamesIter == texImgCacheNames.end()) {
			stbi_image_free(storedImage.second);
		}
	}
	imagesToStore.clear();
	glDeleteTextures(1, &id);
}


/**
* TextureArray::StoreImage
*/
void megamol::msmcluster::TextureArray::StoreImages(int additionalSpace, bool createArray) {
	// Set size of the array appropriately
	size = imagesToStore.size() + additionalSpace;
	if (createArray) {
		createTextureArray();
		addedToStorageIdx = 0;
	} 
	for (int i = addedToStorageIdx; i < imagesToStore.size(); ++i) {
		glBindTexture(GL_TEXTURE_2D_ARRAY, id);
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, width, height, 1,
			GL_RGB, GL_UNSIGNED_BYTE, imagesToStore[i].second);
	}
	addedToStorageIdx = imagesToStore.size();
	
	
}