//needed includes to check for python version
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include <regex>

#include "stdafx.h"
#include "MSMCluster/MSMCluster.h"

#include "mmcore/api/MegaMolCore.std.h"
#include "mmcore/utility/plugins/Plugin200Instance.h"
#include "mmcore/versioninfo.h"
#include "vislib/vislibversion.h"

// Module includes
#include "ClusterRenderer.h"
#include "ScatterPlotRenderer.h"


/* anonymous namespace hides this type from any other object files */
namespace {
	/** Implementing the instance class of this plugin */
	class plugin_instance : public ::megamol::core::utility::plugins::Plugin200Instance {
	public:
		/** ctor */
		plugin_instance(void)
			: ::megamol::core::utility::plugins::Plugin200Instance(

				/* machine-readable plugin assembly name */
				"MSMCluster",

				/* human-readable plugin description */
				"This plugin allows the clustering of MolecularMaps. The clusters can "
				"be shown on the Powerwall. It also allows interaction with via the "
				"tracking system.") {

				// here we could perform addition initialization
				// check for installed python version
				// if there is no python installed on the path
				// we dont even need to bother
				char psBuffer[128];  
				std::regex  re = 'Python 3.*';
				FILE *pPipe;
				//arg "rt" for R eading the output of the command in T extmode
				if( (pPipe = _popen( "python --version", "rt" )) == NULL ){
					exit( 1 );  
				}
				/* Read pipe until end of file, or an error occurs. */  
				while(fgets(psBuffer, 128, pPipe)){  
					printf(psBuffer);  
				}  
			
				/* Close pipe and print return value of pPipe. */  
				if (feof( pPipe)){  
					printf( "\nProcess returned %d\n", _pclose( pPipe ) );  
				}  
				else{  
					vislib::sys::Log::DefaultLog.WriteError( "Error: Failed to read the pipe to the end.\n
																 Could not determine if 'python' installed");
				}
				if(std::regex_match(psBuffer, re)){
					vislib::sys:Log::DefaultLog.WriteInfo("Python found.");
				}else{
					vislib::sys::Log::DefaultLog.WriteError( "Error: python not installed. Please install python >=3.5\n %s", psBuffer);
				}
			

		};
		/** Dtor */
		virtual ~plugin_instance(void) {
			// here we could perform addition de-initialization
		}
		/** Registers modules and calls */
		virtual void registerClasses(void) {
			// Module registration.
			this->module_descriptions.RegisterAutoDescription<megamol::msmcluster::ClusterRenderer>();
			this->module_descriptions.RegisterAutoDescription<megamol::msmcluster::ScatterPlotRenderer>();

		}
		MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_plugininstance_connectStatics
	};
}


/*
 * mmplgPluginAPIVersion
 */
MSMCLUSTER_API int mmplgPluginAPIVersion(void) {
	MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgPluginAPIVersion
}


/*
 * mmplgGetPluginCompatibilityInfo
 */
MSMCLUSTER_API
::megamol::core::utility::plugins::PluginCompatibilityInfo *
mmplgGetPluginCompatibilityInfo(
		::megamol::core::utility::plugins::ErrorCallback onError) {
	// compatibility information with core and vislib
	using ::megamol::core::utility::plugins::PluginCompatibilityInfo;
	using ::megamol::core::utility::plugins::LibraryVersionInfo;

	PluginCompatibilityInfo *ci = new PluginCompatibilityInfo;
	ci->libs_cnt = 2;
	ci->libs = new LibraryVersionInfo[2];

	SetLibraryVersionInfo(ci->libs[0], "MegaMolCore",
		MEGAMOL_CORE_MAJOR_VER, MEGAMOL_CORE_MINOR_VER, MEGAMOL_CORE_COMP_REV, 0
#if defined(DEBUG) || defined(_DEBUG)
		| MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DEBUG_BUILD
#endif
#if defined(MEGAMOL_CORE_DIRTY) && (MEGAMOL_CORE_DIRTY != 0)
		| MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DIRTY_BUILD
#endif
		);

	SetLibraryVersionInfo(ci->libs[1], "vislib",
		vislib::VISLIB_VERSION_MAJOR, vislib::VISLIB_VERSION_MINOR, vislib::VISLIB_VERSION_REVISION, 0
#if defined(DEBUG) || defined(_DEBUG)
		| MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DEBUG_BUILD
#endif
#if defined(VISLIB_DIRTY_BUILD) && (VISLIB_DIRTY_BUILD != 0)
		| MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DIRTY_BUILD
#endif
		);
	//
	// If you want to test additional compatibilties, add the corresponding versions here
	//

	return ci;
}


/*
 * mmplgReleasePluginCompatibilityInfo
 */
MSMCLUSTER_API
void mmplgReleasePluginCompatibilityInfo(
		::megamol::core::utility::plugins::PluginCompatibilityInfo* ci) {
	// release compatiblity data on the correct heap
	MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgReleasePluginCompatibilityInfo(ci)
}


/*
 * mmplgGetPluginInstance
 */
MSMCLUSTER_API
::megamol::core::utility::plugins::AbstractPluginInstance*
mmplgGetPluginInstance(
		::megamol::core::utility::plugins::ErrorCallback onError) {
	MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgGetPluginInstance(plugin_instance, onError)
}


/*
 * mmplgReleasePluginInstance
 */
MSMCLUSTER_API
void mmplgReleasePluginInstance(
		::megamol::core::utility::plugins::AbstractPluginInstance* pi) {
	MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgReleasePluginInstance(pi)
}