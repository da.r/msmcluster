#ifndef MSMCLUSTER_UTILITIES_H_INCLUDED
#define MSMCLUSTER_UTILITIES_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */


#include "vislib/graphics/gl/GLSLShader.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/graphics/gl/ShaderSource.h"

#include "vislib/math/Cuboid.h"
#include "vislib/math/Vector.h"

#include "mmcore/utility/ShaderSourceFactory.h"

#include <string>
#include <iostream>
#include <fstream>


namespace megamol {
namespace msmcluster {
			
	/**
	* Struct used to represent an image. It contains path to the image and the
	* corresponding image descriptor.
	*/
	struct ImageRepresentation {
		std::string path;
		std::vector<double> vec_representation;
			
		bool operator==(const ImageRepresentation &other);
	};

	/**
	* Used in EnumParam in ClusterRenderer and ScatterPlotRenderer modules.
	* Indicates which internal representation of images can be chosen.
	*/
	enum InternalRepresentation {
		GRAYSCALE,
		ALL_CHANNELS
	};

	/**
	* Used in EnumParam in ClusterRenderer and ScatterPlotRenderer modules.
	* Indicates which clustering algorithms can be chosen.
	*/
	enum ClusterAlgo {
		KMEANS,
		DLIB_KMEANS,
		BOTTOM_UP_AGGLOMERATIVE,
		MEAN_SHIFT
	};


	enum ClusterLvl {
		LEVEL1,
		LEVEL2
	};


	/**
	* Initializes buffer, array and element objects.
	*	
	* @param VBO vertex buffer object 
	* @param VAO vertex array object
	* @param EBO element buffer object
	* @param vertices vector of vertices used to initialize the objects
	* @param indices vector of indices used to initialize the objects
	*/
	void bufferArrayObjInitialization(GLuint VBO, GLuint VAO, 
										const std::vector<GLfloat> &vertices, 
										const std::vector<GLuint> &indices);

	/**
	* Given number of clusters computes that many different colors and returns
	* them as a std::vector of 3d vislib vectors.
	*
	* For the first 7 different colors it takes the vertices of the RGB color space
	* excluding black because it is very similar to the background color.
	* For the next 6 different colors it takes colors that correspond to the centers
	* of faces of the RGB color space.
	* If more colors are needed they are computed by going deeper into the cube of the
	* RGB color space.
	*
	* @param numberOfClusters number of clusters,
	* @return colors vector that contains numberOfClusters different colors
	*/
	std::vector<vislib::math::Vector<float, 3> > computeColors(int numberOfClusters);


	/**
	* Compiles vertex and fragment shaders and links them into a shader program.
	*
	* @param shaderProgram used later to represent the shader program
	* @param factory factory object for ShaderSources
	* @param vert vertex shader ShaderSource object used in creation of the shader program
	* @param frag fragment ShaderSource object used in creation of the shader program
	*/
	bool createLinkShaderProgram(vislib::graphics::gl::GLSLShader &shaderProgram,
									core::utility::ShaderSourceFactory *factory,
									vislib::graphics::gl::ShaderSource vert,
									vislib::graphics::gl::ShaderSource frag);

	/**
	* Takes path to an image file, calculates centralized image moments and returns a
	* 10-dimensional vector that contains those image moments and represents a tunnel
	* in the image.
	*
	* @param filepath Path to an image file
	* @return Vector representation of the image (a tuple (proteinID, vector with image moments)
	*/
	ImageRepresentation imageMoments(std::string filepath, bool allChannels);

	/**
	* Converts path string to name string.
	*
	* @param path string that contains some path
	* @return name string that contains the name
	*/
	std::string pathToName(std::string path);

	/**
	* Reads in cached image descriptors
	*
	* @param cacheFileName filename or path to the file containing cached image descriptors
	* @param dimensions dimensions of internal image representation
	* @param readInCache vector that will contain representations of the images that were read in
	*/
	void readCache(std::string cacheFileName, int dimensions, std::vector<ImageRepresentation> &readInCache);

	/**
	* Reads images in a given directory and if they are not in cache converts
	* them to image descriptors. In the course of this fills the given vector
	* of image representations with the image descriptors.
	*
	* @param directory path to the directory that contains images
	* @param allChannels whether all color channels are taken into account when
	* computing image moments
	* @param vec_representations representations of images
	* @param readInCache cache is read into this data structure
	*
	* @return vector containing image descriptors that were not in cache
	*/
	std::vector<ImageRepresentation> readImagesConvertToImageDescriptors(std::string directory, bool allChannels,
		std::vector<ImageRepresentation> &vec_representations, std::vector<ImageRepresentation> &readInCache);

	/**
	* Scales an array of vertices with respect to the original vertices using two scaling
	* factors.
	*
	* @param scale1 scaling factor
	* @param scale2 scaling factor
	* @param vertices vertices that are to be scaled
	* @param origVertices original vertices
	*/
	void scaleVertices(const float scale1, const float scale2, std::vector<GLfloat> &vertices,
						const std::vector<GLfloat> &origVertices);

	/**
	* Writes the given image descriptors to cache.
	*
	* @param data data that is to be written to cache
	* @param cacheFileName name of the cache file to which data should be written
	* @param dimensions dimensions of internal image representation
	*
	* @return 'true' on success, 'false' otherwise.
	*/
	bool writeToCache(std::vector<ImageRepresentation> data, std::string cacheFileName, int dimensions);


} /* end namespace msmcluster */
} /* end namespace megamol */




#endif /* MSMCLUSTER_UTILITIES_H_INCLUDED */