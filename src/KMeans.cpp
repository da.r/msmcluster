#include "stdafx.h"

// Definition because of the conflict with std::numeric_limits
#define NOMINMAX

// Reading images
#include "stb_image.h"

// Contains directory_iterator, C++17 / C++14 with experimental
#include <filesystem>

#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <limits>
#include <utility>
#include <random>
#include <algorithm>

#include "KMeans.h"


namespace megamol {
namespace msmcluster {

		
/*
* KMeans::KMeans
*/
KMeans::KMeans(std::vector< ImageRepresentation > &data, int K = 2)
	: K{ K }, data{ data }, centroids{ K }, nearestToCentroid{ K } {

	std::random_device rand_dev;
	std::mt19937 rand_engine(rand_dev());

	// Initialize current cluster of every vector
	for (int i = 0; i < data.size(); ++i)
		currentClusters.push_back(-1);


	for (int i = 0; i < K; ++i) {
		Cluster_t newCluster;
		clusters.push_back(newCluster);

		int rand_idx = std::uniform_int_distribution<>(0, data.size() - (i + 1))(rand_engine);
		std::vector<double> rand_vector = data.at(rand_idx).vec_representation;
		// Swap now vector on this position with the one at the end
		std::swap(data[rand_idx], data[data.size() - (i + 1)]);
		centroids[i] = rand_vector;
	}

}

/*
* KMeans::getCluster
*/
std::vector<ImageRepresentation	> KMeans::GetCluster(int clusterID)
{
	return clusters.at(clusterID);
}


/*
* KMeans::getClusters
*/
std::vector<KMeans::Cluster_t > KMeans::GetClusters()
{
	return clusters;
}


/*
* KMeans::getNumOfClusters
*/
int KMeans::GetNumOfClusters()
{
	return K;
}


/*
* KMeans::getNearestToCentroid
*/
ImageRepresentation KMeans::GetRepresentativeImage(int clusterID)
{
	return nearestToCentroid[clusterID].first;
}


/*
* KMeans::reassignment
*/
int KMeans::reassignment() {
	int numOfReassigned = 0;

	for (int i = 0; i < K; ++i)
		nearestToCentroid[i] = std::make_pair(ImageRepresentation{ "", std::vector<double>() }, std::numeric_limits<double>::max());

	for (int i = 0; i < K; ++i)
		clusters[i].clear();

	for (int i = 0; i < data.size(); ++i) {
		// Pair contains nearest centroid (its clusterID) and distance to it
		std::pair<int, double> nearest_centroid = std::make_pair(-2, std::numeric_limits<double>::max());

		for (int clusterID = 0; clusterID < clusters.size(); ++clusterID) {

			// Calculate vector to centroid distance; 
			// To compare distances we don't need to take a sqrt
			double vec_to_centr = 0;
			for (int j = 0; j < data[i].vec_representation.size(); ++j) {
				vec_to_centr += abs(data[i].vec_representation[j] - centroids[clusterID][j]);
			}

			if (vec_to_centr < nearest_centroid.second) {
				nearest_centroid = std::make_pair(clusterID, vec_to_centr);
				if (vec_to_centr < nearestToCentroid[clusterID].second) {
					nearestToCentroid[clusterID] = std::make_pair(data[i], vec_to_centr);
				}
			}

		}

		if (nearest_centroid.first != currentClusters[i]) {
			numOfReassigned += 1;
			clusters[nearest_centroid.first].push_back(data[i]);
			currentClusters[i] = nearest_centroid.first;
		}
		else {
			clusters[nearest_centroid.first].push_back(data[i]);
		}
	}

	return numOfReassigned;
}


/*
* KMeans::recomputation
*/
void KMeans::recomputation() {
	std::vector< double > vec_sums(centroids[0].size());

	for (int clusterID = 0; clusterID < K; ++clusterID) {
		for (int dim = 0; dim < centroids[0].size(); ++dim) {
			vec_sums[dim] = 0;
			for (auto vec : clusters[clusterID]) {
				vec_sums[dim] += vec.vec_representation[dim];
			}
			vec_sums[dim] = vec_sums[dim] / clusters[clusterID].size();
		}

		centroids[clusterID] = vec_sums;
	}
}


/*
* KMeans::run
*/
void KMeans::Run() {
	int nuof_reassigned = reassignment();
	int count = 0;
	while (nuof_reassigned > 0) {
		recomputation();
		count += 1;

		std::cout << "Iteration: " << count << " Number of reassignments: " << nuof_reassigned << std::endl;
		for (int i = 0; i < clusters.size(); ++i) {
			std::cout << "\tCluster " << i + 1 << " size:" << clusters[i].size() << std::endl;
			for (int j = 0; j < clusters[i].size(); ++j) {
				std::cout << "\t\t" << clusters[i][j].path << std::endl;
			}
		}
		nuof_reassigned = reassignment();
	}
}


} /* end namespace msmcluster */
} /* end namespace megamol */