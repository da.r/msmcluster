#include "stdafx.h"

#include "BottomUpAgglomerative.h"
#include <cstdlib>
#include <limits>
#include <random>
#include <fstream>

namespace megamol {
namespace msmcluster {

		/**
		* BottomUpAgglomerative::BottomUpAgglomerative
		*/
		BottomUpAgglomerative::BottomUpAgglomerative(std::vector<ImageRepresentation> &data, int K = 2)
			: data{ data }, K{ K }
		{
			// Samples should be in the same order as they are in data
			for (int i = 0; i < data.size(); ++i) {
				SampleType sample;
				sample.set_size(data[i].vec_representation.size());
				for (int j = 0; j < data[i].vec_representation.size(); ++j) {
					sample(j) = data[i].vec_representation[j];
				}
				samples.push_back(sample);
			}

			// Store distances in a matrix
			distances = dlib::matrix<double>(samples.size(), samples.size());
			for (int i = 0; i < distances.nr(); ++i) {
				for (int j = 0; j < distances.nc(); ++j) {
					distances(i, j) = dlib::length(samples[i] - samples[j]);
				}
			}
		}


		/**
		* BottomUpAgglomerative::GetCluster
		*/
		std::vector<ImageRepresentation> BottomUpAgglomerative::GetCluster(int clusterID) {
			return clusters.at(clusterID);
		}


		/**
		* BottomUpAgglomerative::GetClusters
		*/
		std::vector<Cluster_t> BottomUpAgglomerative::GetClusters() {
			return clusters;
		}


		/**
		* BottomUpAgglomerative::GetNumOfClusters
		*/
		int BottomUpAgglomerative::GetNumOfClusters() {
			return K;
		}


		/**
		* BottomUpAgglomerative::GetRepresentativeImage
		*/
		ImageRepresentation BottomUpAgglomerative::GetRepresentativeImage(int clusterID) {
			return representativeImages[clusterID];
		}


		/**
		* BottomUpAgglomerative::Run
		*/
		void BottomUpAgglomerative::Run() {
			std::vector<unsigned long> labels;
			K = dlib::bottom_up_cluster(distances, labels, K);

			// Fill the clusters
			clusters = std::vector<Cluster_t>(K);
			for (int i = 0; i < labels.size(); ++i) {
				clusters[labels[i]].push_back(data[i]);
			}

			// To pick a random element from the cluster
			std::random_device randDev;
			std::mt19937 randEngine(randDev());
			// Store random images as representative images
			for (int clus = 0; clus < clusters.size(); ++clus) {
				int randIdx = std::uniform_int_distribution<>(0, clusters[clus].size() - 1)(randEngine);
				representativeImages.push_back(clusters[clus][randIdx]);
			}
		}


} /* end namespace msmcluster */
} /* end namespace megamol */