#include "stdafx.h"

#include "vislib/graphics/gl/GLSLShader.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/graphics/gl/ShaderSource.h"

#include "vislib/math/Cuboid.h"
#include "vislib/math/Vector.h"

#include "mmcore/utility/ShaderSourceFactory.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <future>
#include <iostream>
#include <string>




#include "stb_image.h"
#include "Utilities.h"

using namespace megamol;


/**
* msmcluster::ImageRepresentation::operator==
*/
bool msmcluster::ImageRepresentation::operator==(const ImageRepresentation &other) {
	return path == other.path && vec_representation == other.vec_representation;
}


/**
* msmcluster::bufferArrayObjInitialization
*/
void msmcluster::bufferArrayObjInitialization(GLuint VBO, GLuint VAO, const std::vector<GLfloat> &vertices, const std::vector<GLuint> &indices)
{
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void *)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);
}


/**
* msmcluster::computeColors
*/
std::vector<vislib::math::Vector<float, 3> > msmcluster::computeColors(int numberOfClusters) {

	vislib::math::Cuboid<float> rgbSpace(vislib::math::Point<float, 3>(0.0f, 0.0f, 0.0f), vislib::math::Dimension<float, 3>(1.0f, 1.0f, 1.0f));

	std::vector<vislib::math::Vector<float, 3> > result;
	// 6 faces and 7 vertices of the RGB model space 
	// (without black, because that is the background color)
	float spaceDepth = std::ceil(numberOfClusters / (7.0f + 6.0f));
	float currentDepth = spaceDepth;

	for (int i = 0; i < numberOfClusters; ++i) {

		// Go through the vertices
		if (i % 13 == 0)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetLeftBottomFront()));
		else if (i % 13 == 1)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetRightBottomBack()));
		else if (i % 13 == 2)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetLeftTopBack()));
		else if (i % 13 == 3)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetLeftTopFront()));
		else if (i % 13 == 4)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetRightBottomFront()));
		else if (i % 13 == 5)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetRightTopBack()));
		else if (i % 13 == 6)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(rgbSpace.GetRightTopFront()));

		// Go through the centers of faces
		else if (i % 13 == 7)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(0.5f, 0.5f, 0.0f));
		else if (i % 13 == 8)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(0.5f, 0.0f, 0.5f));
		else if (i % 13 == 9)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(0.0f, 0.5f, 0.5f));
		else if (i % 13 == 10)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(0.5f, 0.5f, 1.0f));
		else if (i % 13 == 11)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(1.0f, 0.5f, 0.5f));
		else if (i % 13 == 12)
			result.push_back((currentDepth / spaceDepth) * vislib::math::Vector<float, 3>(0.5f, 1.0f, 0.5f));

		if (i % 13 == 0 && i > 12)
			--currentDepth;
	}

	return result;
}


/**
* msmcluster::createLinkShaderProgram
*/
bool msmcluster::createLinkShaderProgram(vislib::graphics::gl::GLSLShader &shaderProgram,
	core::utility::ShaderSourceFactory *factory,
	vislib::graphics::gl::ShaderSource vert,
	vislib::graphics::gl::ShaderSource frag) {
	if (!factory->MakeShaderSource("MSMCluster::simple::vert", vert)) return false;
	if (!factory->MakeShaderSource("MSMCluster::simple::frag", frag)) return false;

	try {
		if (!shaderProgram.Create(vert.Code(), vert.Count(), frag.Code(), frag.Count())) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
				"Unable to compile %s: unknown error\n", "MSMCluster");
			return false;
		}
		if (!shaderProgram.Link()) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
				"Unable to link %s: Unknown error\n", "MSMCluster");
			return false;
		}

	}
	catch (vislib::graphics::gl::AbstractOpenGLShader::CompileException ce) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
			"unable to compile %s (@%s): %s\n", "MSMCluster",
			vislib::graphics::gl::AbstractOpenGLShader::CompileException::CompileActionName(
				ce.FailedAction()), ce.GetMsgA());
		return false;

	}
	catch (vislib::Exception e) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
			"unable to compile %s: %s\n", "MSMCluster", e.GetMsgA());
		return false;

	}
	catch (...) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
			"unable to compile %s: unknown exception\n", "MSMCluster");
		return false;
	}
	return true;
}


/*
* msmcluster::ImageMoments
*/
msmcluster::ImageRepresentation msmcluster::imageMoments(std::string filepath, bool allChannels) {
	// Image moments needed for centroid calculation
	int M00 = 0, M10 = 0, M01 = 0;

	std::cout << "Started computing image moments of: " << filepath << std::endl;

	// Reading pixel intensities and filling image moments needed for centroid calculation
	int width, height, nrChannels;
	std::string proteinPath = filepath;

	int desiredChannels = 1;
	if (allChannels)
		desiredChannels = 3;
	unsigned char *img = stbi_load(filepath.c_str(), &width, &height, &nrChannels, desiredChannels);

	std::vector<double> imVector;
	for (int i = 0; i < desiredChannels; ++i) {

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int pixel_intensity = static_cast<int>(img[x + i + y*width]);
				M00 += pixel_intensity;
				M10 += x * pixel_intensity;
				M01 += y * pixel_intensity;
			}
		}
		int xc = 0;
		int yc = 0;

		// Components of the centroid
		if (M00 != 0) {
			int xc = M10 / M00;
			int yc = M01 / M00;
		}

		// Centralized image moments
		double mu00 = M00;
		double mu11 = 0, mu02 = 0, mu20 = 0, mu12 = 0, mu21 = 0, mu03 = 0, mu30 = 0;

		// Calculating centralized image moments
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int pixel_intensity = static_cast<int>(img[x + i + y*width]);
				mu11 += (x - xc) * (y - yc) * pixel_intensity;
				mu02 += pow(y - yc, 2) * pixel_intensity;
				mu20 += pow(x - xc, 2) * pixel_intensity;
				mu12 += (x - xc) * pow(y - yc, 2) * pixel_intensity;
				mu21 += pow(x - xc, 2) * (y - yc) * pixel_intensity;
				mu03 += pow(y - yc, 3) * pixel_intensity;
				mu30 += pow(x - xc, 3) * pixel_intensity;
			}
		}
		std::vector<double> colorVector = { mu00, 0, 0, mu11, mu02, mu20, mu12, mu21, mu03, mu30 };
		imVector.insert(imVector.end(), colorVector.begin(), colorVector.end());
	}
	ImageRepresentation vec_representation{ proteinPath, imVector };


	// Deallocating resources
	stbi_image_free(img);

	return vec_representation;
}


/**
* msmcluster::pathToName
*/
std::string msmcluster::pathToName(std::string path) {
	size_t lastDot = path.find_last_of(".");
	size_t lastSlash = path.find_last_of("\\");
	if (lastSlash == std::string::npos) {
		lastSlash = path.find_last_of("/");
	}
	return path.substr(lastSlash, lastDot);
}


/**
* msmcluster::readCache
*/
void msmcluster::readCache(std::string cacheFileName, int dimensions, std::vector<ImageRepresentation> &readInCache) {
	std::ifstream inf(cacheFileName, std::ios::binary);
	if (!inf) {
		std::cerr << "Could not read " << cacheFileName << std::endl;
		return;
	}

	do {
		std::vector<double> imVec(dimensions);
		std::string path;
		std::getline(inf, path, '\0');
		size_t lineSize = imVec.size() * sizeof(imVec[0]);
		// std::cout << fileSize << std::endl;
		inf.read(reinterpret_cast<char*>(&imVec[0]), lineSize);
		// std::cout << "READING: Print contents of imVec: " << std::endl;
		/*for (int i = 0; i < 10; ++i) {
		std::cout << imVec[i] << std::endl;
		}*/
		readInCache.push_back(ImageRepresentation{ path, imVec });
	} while (inf);

}


/*
* ClusterRenderer::readImagesConvertToImageDescriptors
*/
std::vector<msmcluster::ImageRepresentation> megamol::msmcluster::readImagesConvertToImageDescriptors(std::string directory, bool allChannels, std::vector<ImageRepresentation> &vec_representations, std::vector<ImageRepresentation> &readInCache) {

	std::vector<std::future<ImageRepresentation> > wereNotInCacheFtr;
	std::vector<ImageRepresentation> wereNotInCache;

	for (auto &file : std::experimental::filesystem::directory_iterator(directory)) {
		if (file.path().extension() == ".png") {
			std::string filePath = file.path().string();

			auto predicate = [filePath](const ImageRepresentation &im) {return im.path == filePath; };
			auto it = std::find_if(readInCache.begin(), readInCache.end(), predicate);
			if (it != readInCache.end())
				vec_representations.push_back(*it);
			else {
#if defined(DEBUG) || defined(_DEBUG)
				wereNotInCacheFtr.push_back(std::async(std::launch::async, imageMoments, file.path().string(), allChannels));
#else
				// Commented because of race condition in release mode
				// wereNotInCacheFtr.push_back(std::async(std::launch::async, imageMoments, file.path().string(), allChannels));
				wereNotInCache.push_back(imageMoments(file.path().string(), allChannels));
#endif
			}

		}
	}
	// Commented because of race condition in release mode
	/*for (int i = 0; i < wereNotInCacheFtr.size(); ++i) {
	wereNotInCache.push_back(wereNotInCacheFtr[i].get());
	}*/

#if defined(DEBUG) || defined(_DEBUG)
	for (int i = 0; i < wereNotInCacheFtr.size(); ++i) {
		wereNotInCache.push_back(wereNotInCacheFtr[i].get());
	}
#endif
	return wereNotInCache;
}


/**
* msmcluster::scaleVertices
*/
void msmcluster::scaleVertices(const float scale1, const float scale2, std::vector<GLfloat> &vertices, const std::vector<GLfloat> &origVertices) {
	for (int i = 0; i < 4; ++i) {
		vertices[i * 5] = scale1 * scale2 * origVertices[i * 5];
		vertices[i * 5 + 1] = scale1 * scale2 * origVertices[i * 5 + 1];
		vertices[i * 5 + 2] = scale1 * scale2 * origVertices[i * 5 + 2];
	}
}


/*
* ClusterRenderer::writeToCache
*/
bool megamol::msmcluster::writeToCache(std::vector<ImageRepresentation> data, std::string cacheFileName, int dimensions) {
	// Open the cache 
	std::ofstream outf(cacheFileName, std::ios::app | std::ios::binary);
	if (!outf) {
		std::cerr << "Output file could not be created/opened.\n";
		return false;
	}
	for (int i = 0; i < data.size(); ++i) {
		outf.write(data[i].path.c_str(), data[i].path.size());
		// debug - \0 has to be a string, not a char!
		outf.write("\0", sizeof(char));
		outf.write(reinterpret_cast<const char*>(&(data[i].vec_representation[0])), data[i].vec_representation.size() * sizeof(data[i].vec_representation[0]));

	}
	return true;
}