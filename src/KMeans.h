#ifndef MSMCLUSTER_KMEANS_H_INCLUDED
#define MSMCLUSTER_KMEANS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "IClassifier.h"


namespace megamol {
namespace msmcluster {

	/**
	* An implementation of k-means clustering
	*/
	class KMeans : public IClassifier {
	
	public:
		/**
		* Takes path to the data and K, number of wanted clusters.
		* Initializes k-means by selecting random initial centroids.
		*
		* @param vec_representations: data to cluster
		* @param K: number of clusters
		*/
		KMeans(std::vector< ImageRepresentation> &data, int K);

		/**
		* Getter for a vector that contains ImageRepresentationss of all the images in
		* a cluster with the given cluster id.
		*
		* @return numOfClusters
		*/
		std::vector<ImageRepresentation> KMeans::GetCluster(int clusterID);

		/**
		* Getter for a std::vector of clusters.
		*
		* @return clusters two dimensional vector that constains clusters
		*/
		std::vector<Cluster_t> GetClusters();

		/**
		* Returns the representation of the image that is nearest to the centroid in cluster with 
		* a clusterID.
		*
		* @param clusterID: ID of the cluster
		* @return nearestToCentroid: name of the image nearest to the centroid
		*/
		ImageRepresentation GetRepresentativeImage(int clusterID);

		/**
		* Getter for the number of clusters
		*
		* @return numOfClusters how many clusters are there
		*/
		int GetNumOfClusters();

		/**
		* Starts the clustering
		*/
		void Run();

	private:

		// Number of clusters
		int K = 2;

		using Cluster_t = std::vector< ImageRepresentation>;

		std::vector< std::vector<double> > centroids;
		std::vector< std::pair<ImageRepresentation, double> > nearestToCentroid;
		std::vector< ImageRepresentation > data;

		std::vector<Cluster_t> clusters;
		std::vector<int> currentClusters;

		/**
		* Reassigns each vector to its closest centroid and returns the
		* number of reassignments.
		*
		* @return num_of_reassigned: number of reassigned vectors
		*/
		int reassignment();

		/**
		* Recomputes each centroid
		*/
		void recomputation();

	};


} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_KMEANS_H_INCLUDED */
