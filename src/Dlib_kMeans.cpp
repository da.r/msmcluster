#include "stdafx.h"

#include "Dlib_kMeans.h"
#include <cstdlib>
#include <limits>
#include <random>
#include <fstream>

namespace megamol {
namespace msmcluster {


		/**
		* Dlib_kMeans::Dlib_kMeans
		*/
		Dlib_kMeans::Dlib_kMeans(std::vector<ImageRepresentation> &data, int K = 2)
			: data{ data }, K{ K }
		{
			// Samples should be in the same order as they are in data
			for (int i = 0; i < data.size(); ++i) {
				SampleType sample;
				sample.set_size(data[i].vec_representation.size());
				for (int j = 0; j < data[i].vec_representation.size(); ++j) {
					sample(j) = data[i].vec_representation[j];
				}
				samples.push_back(sample);
			}

			for (int i = 0; i < K; ++i) {
				clusters.push_back(std::vector<ImageRepresentation>());
			}

		}


		/**
		* Dlib_kMeans::GetCluster
		*/
		std::vector<ImageRepresentation> Dlib_kMeans::GetCluster(int clusterID) {
			return clusters.at(clusterID);
		}


		/**
		* Dlib_kMeans::GetClusters
		*/
		std::vector<Cluster_t> Dlib_kMeans::GetClusters() {
			return clusters;
		}


		/**
		* Dlib_kMeans::GetNumOfClusters
		*/
		int Dlib_kMeans::GetNumOfClusters() {
			return K;
		}


		/**
		* Dlib_kMeans::GetRepresentativeImage
		*/
		ImageRepresentation Dlib_kMeans::GetRepresentativeImage(int clusterID) {
			return nearestToCentroid.at(clusterID);
		}


		/**
		* Dlib_kMeans::Run
		*/
		void Dlib_kMeans::Run() {
			dlib::pick_initial_centers(K, centroids, samples);

			dlib::find_clusters_using_kmeans(samples, centroids);

			std::vector<std::pair<int, double> > indexesOfNearestToCentroid;

			// Initialize distances in indexesOfNearestToCentroid 
			for (int i = 0; i < K; ++i) {
				indexesOfNearestToCentroid.push_back(std::make_pair(-1, 9e99));
			}

			for (int i = 0; i < samples.size(); ++i) {
				int nearestCentroid = 0;
				double distToNearest = 9e99;
				for (int j = 0; j < centroids.size(); ++j) {
					double distance = dlib::length(samples[i] - centroids[j]);
					if (distance < distToNearest) {
						distToNearest = distance;
						nearestCentroid = j;
					}
					if (distance < indexesOfNearestToCentroid[j].second) {
						indexesOfNearestToCentroid[j] = std::make_pair(i, distance);
					}
				}
				clusters[nearestCentroid].push_back(data[i]);
			}


			for (int i = 0; i < K; ++i) {
				nearestToCentroid.push_back(data[indexesOfNearestToCentroid[i].first]);
			}

			std::cout << "USING DLIB\n";
			for (int i = 0; i < K; ++i) {
				std::cout << "\n\nCluster " << i << std::endl;
				for (auto &elt : clusters[i]) {
					std::cout << "\t" << elt.path << " \n";
				}
			}

		}


} /* end namespace msmcluster */
} /* end namespace megamol */