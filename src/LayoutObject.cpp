#include "stdafx.h"

#include "LayoutObject.h"


/**
* LayoutObject::LayoutObject
*/
megamol::msmcluster::LayoutObject::LayoutObject(ImageRepresentation imageProperties, ClusterLvl level)
	: level{ level }
{
	this->imageProperties = imageProperties;
}


/**
* LayoutObject::Distance
*/
float megamol::msmcluster::LayoutObject::Distance(LayoutObject &other) {
	float tmp = 0;
	for (int i = 0; i < imageProperties.vec_representation.size(); ++i) {
		tmp += (imageProperties.vec_representation.at(i) - other.imageProperties.vec_representation.at(i))
			* (imageProperties.vec_representation.at(i) - other.imageProperties.vec_representation.at(i));
	}
	return sqrt(tmp);
}


/**
* LayoutObject::Weight
*/
float megamol::msmcluster::LayoutObject::Weight(LayoutObject &other) {

	if (level == ClusterLvl::LEVEL1 && other.level == ClusterLvl::LEVEL1)
		return 0.5f;

	if (level == ClusterLvl::LEVEL2 || other.level == ClusterLvl::LEVEL2)
		return 10.0f;

	return 0.0f;
}
