#include "stdafx.h"

#include "ScatterPlotRenderer.h"


// MegaMol includes
#include "mmcore/CoreInstance.h"
#include "mmcore/param/ButtonParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/FilePathParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/view/CallRender2D.h"


// vislib includes
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/math/FastMap.h"


#include <future>
#include <filesystem>

/*
* ScatterPlotRenderer::~ScatterPlotRenderer
*/
megamol::msmcluster::ScatterPlotRenderer::~ScatterPlotRenderer() {
	this->Release();
}


/*
* ScatterPlotRenderer::ScatterPlotRenderer
*/
megamol::msmcluster::ScatterPlotRenderer::ScatterPlotRenderer() :megamol::core::view::Renderer2DModule(),
paramSelectionButtons("selection::buttons", "The button mask for selecting clusters via a 6 DOF device."),
slotGet6DofState("get6DofState", "Retrieves updates from a 6 DOF device."), 
paramImageRepresentation("clustering::imageRepresentation", "Allows the user to choose the internal representation of the image used when clustering."),
paramClusteringAlgo("clustering::clusteringAlgo", "Allows the user to choose the clustering algorithm."),
paramNumOfClusters("clustering::numberOfClusters", "Data is clustered in this number of clusters."), 
paramCluster("clustering::cluster", "Cluster the data again"), 
paramImagesPath("clustering::imagesPath", "Path to the folder that contains the images."),
paramPointSizeScalling("visualization::pointSizeScalling", "Change the point size. Default value is 5."),
paramSelectedPoint("visualization::selectedImage", "Image that is behind the selected point") {

	// Activate the param slot for the selection button.
	this->paramSelectionButtons << new core::param::IntParam(0x03);
	this->MakeSlotAvailable(&this->paramSelectionButtons);

	// Activate the param slot for the tracking system.
	this->slotGet6DofState.SetCompatibleCall<
		megamol::core::view::Call6DofInteractionDescription>();
	this->MakeSlotAvailable(&this->slotGet6DofState);

	core::param::EnumParam *representations = new core::param::EnumParam(0);
	representations->SetTypePair(InternalRepresentation::GRAYSCALE, "imageMoments_grayscale");
	representations->SetTypePair(InternalRepresentation::ALL_CHANNELS, "imageMoments_allColorChannels");
	this->paramImageRepresentation << representations;
	this->MakeSlotAvailable(&this->paramImageRepresentation);

	core::param::EnumParam *algos = new core::param::EnumParam(0);
	algos->SetTypePair(ClusterAlgo::KMEANS, "kMeans");
	algos->SetTypePair(ClusterAlgo::DLIB_KMEANS, "dlib::kMeans");
	algos->SetTypePair(ClusterAlgo::BOTTOM_UP_AGGLOMERATIVE, "dlib::bottomUpAgglomerative");
	algos->SetTypePair(ClusterAlgo::MEAN_SHIFT, "MeanShift (in python)");
	this->paramClusteringAlgo << algos;
	this->MakeSlotAvailable(&this->paramClusteringAlgo);

	this->paramImagesPath << new core::param::FilePathParam("../plugins/MSMCluster/resources/");
	this->MakeSlotAvailable(&this->paramImagesPath);

	this->paramNumOfClusters << new core::param::IntParam(0x04);
	this->MakeSlotAvailable(&this->paramNumOfClusters);

	this->paramCluster << new core::param::ButtonParam(vislib::sys::KeyCode::KEY_ENTER);
	this->paramCluster.SetUpdateCallback(&ScatterPlotRenderer::onParamClusterPressed);
	this->MakeSlotAvailable(&this->paramCluster);

	this->paramPointSizeScalling << new core::param::IntParam(pointSizeScalling);
	this->MakeSlotAvailable(&this->paramPointSizeScalling);

	this->paramSelectedPoint << new core::param::StringParam("no point selected");
	this->MakeSlotAvailable(&this->paramSelectedPoint);
}


/*
* ScatterPlotRenderer::create
*/
bool megamol::msmcluster::ScatterPlotRenderer::create(void) {
	
	return true;
}


/*
* ScatterPlotRenderer::get6DofState
*/
megamol::core::view::Call6DofInteraction* megamol::msmcluster::ScatterPlotRenderer::get6DofState(void) {
	auto c = this->slotGet6DofState.CallAs<megamol::core::view::Call6DofInteraction>();
	if ((c != nullptr) && (*c)(megamol::core::view::Call6DofInteraction::IDX_GET_STATE)) {
		return c;
	} else {
		return nullptr;
	}
}


/*
* ScatterPlotRenderer::GetCapabilities
*/
bool megamol::msmcluster::ScatterPlotRenderer::GetCapabilities(core::Call& call) {
	megamol::core::view::CallRender2D* cr = dynamic_cast<megamol::core::view::CallRender2D*>(&call);
	if (cr == NULL) return false;

	return true;
}


/*
* ScatterPlotRenderer::GetExtents
*/
bool megamol::msmcluster::ScatterPlotRenderer::GetExtents(megamol::core::view::CallRender2D& call) {
	call.SetBoundingBox(minX, minY, maxX, maxY);
	return true;
}


/*
* ScatterPlotRenderer::MouseEvent
*/
bool megamol::msmcluster::ScatterPlotRenderer::MouseEvent(float x, float y, ::megamol::core::view::MouseFlags flags) {
	// HINT: The x and y are the current position of the mouse.
	// HINT: The flags can be checked like this: if (flags & megamol::core::view::MOUSEFLAG_MODKEY_CTRL_DOWN)
	//       There are more flags, just use the flags you want. In the infovis::NGParallelCoordinatesRenderer2D
	//       is an example of how to do this.
	
	// Selecting
	if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_CHANGED) {
		if (flags & megamol::core::view::MOUSEFLAG_BUTTON_LEFT_DOWN) {

			int cnt = 0;
			for (int clus = 0; clus < numberOfClusters; ++clus) {

				for (int i = 0; i < classifier->GetCluster(clus).size(); ++i) {

					float xCorrection = x - destination[cnt].GetX();
					float yCorrection = y - destination[cnt].GetY();

					// If it is within one percent (scaled with the point size)
					float withinX = pointSizeScalling * 0.01 * (maxX - minX);
					float withinY = pointSizeScalling * 0.01 * (maxY - minY);
					
					if (abs(xCorrection) < withinX) {
						if (abs(yCorrection) < withinY) {
							core::param::StringParam *atSelectedImageParam = this->paramSelectedPoint.Param<core::param::StringParam>();
							std::string pathToImage = classifier->GetCluster(clus)[i].path;
							
							std::string fileName = pathToName(pathToImage);
							
							atSelectedImageParam->SetValue(fileName.c_str());
							return true;
						}
					}
					
					++cnt;

				}

			}
		}
	}
	return false;
}


/*
* ScatterPlotRenderer::release
*/
void megamol::msmcluster::ScatterPlotRenderer::release(void) {
	glDeleteLists(displayList, numberOfClusters);
}


/*
* ScatterPlotRenderer::Render
*/
bool megamol::msmcluster::ScatterPlotRenderer::Render(megamol::core::view::CallRender2D& call) {

	// Button mask for selecting.
	auto selectionButtons = static_cast<
		megamol::core::view::Call6DofInteraction::ButtonMaskType>(
			this->paramSelectionButtons.Param<core::param::IntParam>()->Value());

	// Get the current state of the Stick and apply the rotation to the current
	// camera. Also check if the user clicked on something.
	auto callTrackerState = this->get6DofState();
	if ((callTrackerState != nullptr) && callTrackerState->IsValid()) {
		
	}

	if (bboxSizeChanged) {
		auto resetView = this->FindNamedObject("::inst::View2D1")
							 ->GetCoreInstance()
							 ->FindParameter("::inst::View2D1::resetView");

		resetView->setDirty();
		bboxSizeChanged = false;
	}


	if (paramClusterPressed) {
		paramClusterPressed = false;

		// Delete display lists from the old instance before we update numberOfClusters
		glDeleteLists(displayList, numberOfClusters);

		// Check whether the user changed number of clusters
		if (this->paramNumOfClusters.IsDirty()) {
			core::param::IntParam *atNumOfClustersParam = this->paramNumOfClusters.Param<core::param::IntParam>();
			numberOfClusters = atNumOfClustersParam->Value();
			clusterColors = computeColors(numberOfClusters);
			paramPointSizeScalling.ResetDirty();
		}

		cluster();

		// Make as many display lists as there are clusters
		int cnt = 0;
		displayList = glGenLists(numberOfClusters);
		for (int clus = 0; clus < numberOfClusters; ++clus) {
			glNewList(displayList + clus, GL_COMPILE);
			glColor3ub(clusterColors.at(clus).GetX() * 255, clusterColors.at(clus).GetY() * 255, clusterColors.at(clus).GetZ() * 255);
			glBegin(GL_POINTS);
			for (int i = 0; i < classifier->GetCluster(clus).size(); ++i) {

				glVertex2f(destination[cnt].GetX(), destination[cnt].GetY());
				++cnt;

			}
			glEnd();
			glEndList();
		}

		clustered = true;
	}

	if (paramPointSizeScalling.IsDirty()) {

		core::param::IntParam *atPointSizeParam = this->paramPointSizeScalling.Param<core::param::IntParam>();
		pointSizeScalling = atPointSizeParam->Value();
		paramPointSizeScalling.ResetDirty();
	}
	
	
	glPointSize(pointSizeScalling);
	
	if (clustered) {
		for (int i = 0; i < numberOfClusters; ++i) {
			glCallList(displayList + i);
		}
		
	}
	return true;
}


void megamol::msmcluster::ScatterPlotRenderer::cluster() {

	// Check which algo is chosen
	auto clusterAlgo = this->paramClusteringAlgo.Param<core::param::EnumParam>()->Value();

	std::string path = this->paramImagesPath.Param<core::param::FilePathParam>()->Value();

	// reset positions
	destination.Clear();

	if (clusterAlgo == ClusterAlgo::MEAN_SHIFT) {
		classifier.reset(new MeanShift(path));
		for (auto &cluster : classifier->GetClusters()) {
			for (int i = 0; i < cluster.size(); ++i) {
				destination.Add(vislib::math::Vector<float, 2>(cluster[i].vec_representation[0],
					cluster[i].vec_representation[1]));
			}
		}

		// Store found number of clusters
		numberOfClusters = classifier->GetClusters().size();
	} else {
		std::vector<ImageRepresentation> vec_representations;

		// Check the chosen internal representation of images
		auto internalRepresentation = this->paramImageRepresentation.Param<core::param::EnumParam>()->Value();
		int internalReprDimensions;
		std::string cacheFileName = cacheFileNameGray;
		switch (internalRepresentation) {
		case InternalRepresentation::GRAYSCALE:
			allChannels = false;
			internalReprDimensions = 10;
			break;
		case InternalRepresentation::ALL_CHANNELS:
			allChannels = true;
			cacheFileName = cacheFileNameColors;
			internalReprDimensions = 30;
			break;
		default:
			allChannels = false;
			internalReprDimensions = 10;
		}

		clusterColors = computeColors(numberOfClusters);

		readInCache.clear();
		readCache(cacheFileName, internalReprDimensions, readInCache);

		std::vector<ImageRepresentation> wereNotInCache = readImagesConvertToImageDescriptors(path, allChannels, vec_representations, readInCache);

		writeToCache(wereNotInCache, cacheFileName, internalReprDimensions);

		readInCache.insert(readInCache.end(), wereNotInCache.begin(), wereNotInCache.end());
		vec_representations.insert(vec_representations.end(), wereNotInCache.begin(), wereNotInCache.end());

		// Handle the case where the numOfClusters > number of images
		if (numberOfClusters > vec_representations.size()) {
			core::param::IntParam *atNumOfClustersParam = this->paramNumOfClusters.Param<core::param::IntParam>();
			numberOfClusters = vec_representations.size();
			atNumOfClustersParam->SetValue(numberOfClusters);
			warningMessage = true;
		}

		std::cout << "Finished Reading" << std::endl;

		switch (clusterAlgo) {
		case ClusterAlgo::KMEANS:
			classifier.reset(new KMeans(vec_representations, numberOfClusters));
			break;
		case ClusterAlgo::DLIB_KMEANS:
			classifier.reset(new Dlib_kMeans(vec_representations, numberOfClusters));
			break;
		case ClusterAlgo::BOTTOM_UP_AGGLOMERATIVE:
			classifier.reset(new BottomUpAgglomerative(vec_representations, numberOfClusters));

		default:
			classifier.reset(new KMeans(vec_representations, numberOfClusters));
		}

		classifier->Run();
		for (int i = 0; i < classifier->GetNumOfClusters(); ++i) {
			std::cout << "Representative image of the cluster " << i + 1 << ": " << classifier->GetRepresentativeImage(i).path << std::endl;
		}


		source.Clear();
		int cnt = 0;
		for (int clus = 0; clus < numberOfClusters; ++clus) {
			for (int i = 0; i < classifier->GetCluster(clus).size(); ++i) {
				ImageRepresentation imageProperties = classifier->GetCluster(clus).at(i);
				LayoutObject obj(imageProperties, ClusterLvl::LEVEL1);
				source.Add(obj);
			}
		}
		
		destination.SetCount(source.Count());

		vislib::math::FastMap<LayoutObject, float, 2> fastMap(source, destination);

		if (warningMessage)
			std::cerr << "WARNING: Number of clusters was set to a number larger "
			<< "than the number of the images. It is now adjusted to the number of images in the folder.\n";
	}

	// Reset the bounds
	minX = 1e100, minY = 1e100, maxX = -1e100, maxY = -1e100;
	// Find the bounds
	for (int i = 0; i < destination.Count(); ++i) {
		if (destination[i].GetX() < minX)
			minX = destination[i].GetX();
		if (destination[i].GetX() > maxX)
			maxX = destination[i].GetX();
		if (destination[i].GetY() < minY)
			minY = destination[i].GetY();
		if (destination[i].GetY() > maxY)
			maxY = destination[i].GetY();
	}
	bboxSizeChanged = true;

	
}

// Callback functions
bool megamol::msmcluster::ScatterPlotRenderer::onParamClusterPressed(megamol::core::param::ParamSlot &paramSlot) {
	paramClusterPressed = true;
	return true;
}