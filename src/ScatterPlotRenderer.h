#ifndef MSMCLUSTER_SCATTERPLOTRENDERER_H_INCLUDED
#define MSMCLUSTER_SCATTERPLOTRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/view/Renderer2DModule.h"
#include "mmcore/Call.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/ParamSlot.h"
#include "mmcore/view/Call6DofInteraction.h"

#include "BottomUpAgglomerative.h"
#include "Dlib_kMeans.h"
#include "KMeans.h"
#include "MeanShift.h"
#include "LayoutObject.h"
#include <dlib/clustering.h>

namespace megamol {
namespace msmcluster {


	/**
	* Renderer for the 2D clusters.
	*/
	class ScatterPlotRenderer : public core::view::Renderer2DModule {
	public:

		/**
		* Answer the name of this module.
		*
		* @return The name of this module.
		*/
		static const char *ClassName(void) {
			return "ScatterPlotRenderer";
		}

		/**
		* Answer a human readable description of this module.
		*
		* @return A human readable description of this module.
		*/
		static const char *Description(void) {
			return "ScatterPlotRenderer 2D MSM clusters.";
		}

		/**
		* Answers whether this module is available on the current system.
		*
		* @return 'true' if the module is available, 'false' otherwise.
		*/
		static bool IsAvailable(void) {
			return true;
		}

		/** Dtor. */
		virtual ~ScatterPlotRenderer(void);

		/** Ctor. */
		ScatterPlotRenderer(void);

	protected:

		/**
		* Implementation of 'Create'.
		*
		* @return 'true' on success, 'false' otherwise.
		*/
		virtual bool create(void);

		/**
		* The get capabilities callback. The module should set the members
		* of 'call' to tell the caller its capabilities.
		*
		* @param call The calling call.
		*
		* @return The return value of the function.
		*/
		virtual bool GetCapabilities(core::Call& call);

		/**
		* The get extents callback. The module should set the members of
		* 'call' to tell the caller the extents of its data (bounding boxes
		* and times).
		*
		* @param call The calling call.
		*
		* @return The return value of the function.
		*/
		virtual bool GetExtents(megamol::core::view::CallRender2D& call);

		/**
		* Callback for mouse events (move, press, and release)
		*
		* @param x The x coordinate of the mouse in world space
		* @param y The y coordinate of the mouse in world space
		* @param flags The mouse flags
		*/
		virtual bool MouseEvent(float x, float y, ::megamol::core::view::MouseFlags flags);

		/**
		* Implementation of 'Release'.
		*/
		virtual void release(void);

		/**
		* The render callback.
		*
		* @param call The calling call.
		*
		* @return The return value of the function.
		*/
		virtual bool Render(megamol::core::view::CallRender2D& call);

	private:

		std::vector<vislib::math::Vector<float, 3> > clusterColors;
		// Cached image moments that are read in
		std::vector<ImageRepresentation > readInCache;
		std::string cacheFileNameGray = "cache_gray.msmcluster";
		std::string cacheFileNameColors = "cache_colors.msmcluster";
		vislib::Array<LayoutObject> source;
		vislib::Array<vislib::math::Vector<float, 2> > destination;
		float minX = 1e30, minY = 1e30, maxX = -1e30, maxY = -1e30;
		std::vector<int> clusterSizes;
		std::vector<int> nearestToCentroids;
		bool warningMessage = false;
		bool bboxSizeChanged = false;

		
		bool clustered = false;
		GLuint displayList;

		std::unique_ptr<IClassifier> classifier;

		// Parameters

		bool allChannels;
		core::param::ParamSlot paramImageRepresentation;

		core::param::ParamSlot paramClusteringAlgo;

		int numberOfClusters = 4;
		core::param::ParamSlot paramNumOfClusters;

		bool paramClusterPressed = false;
		core::param::ParamSlot paramCluster;

		core::param::ParamSlot paramImagesPath;

		int pointSizeScalling = 5;
		core::param::ParamSlot paramPointSizeScalling;

		core::param::ParamSlot paramSelectedPoint;

		/**
		Callback function for the Cluster key.
		*/
		bool onParamClusterPressed(megamol::core::param::ParamSlot &paramSlot);

		void cluster();


		/**
		* Invoke 'slotGet6DofState' to retrieve the current state of the 6DOF
		* device.
		*
		* @return The call or nullptr in case of an error.
		*/
		megamol::core::view::Call6DofInteraction* get6DofState(void);

		/**
		* The buttons that must be pressed on the 6DOF device to select a
		* cluster.
		*/
		core::param::ParamSlot paramSelectionButtons;

		/** Enables the view receiving updates from a 6DOF pointing device. */
		megamol::core::CallerSlot slotGet6DofState;
	};


} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_SCATTERPLOTRENDERER_H_INCLUDED */#pragma once
