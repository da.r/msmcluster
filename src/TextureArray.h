#ifndef MSMCLUSTER_TEXTUREARRAY_H_INCLUDED
#define MSMCLUSTER_TEXTUREARRAY_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include <string>
#include <vector>

namespace megamol {
namespace msmcluster {

	/**
	* Helper class that represents a texture array.
	*/
	class TextureArray {

	public:
		GLuint id;

		/**
		* Creates a new texture array with the given properties.
		*
		* @param width width of textures in the array
		* @param height height of textures in the array
		*/
		TextureArray(int width, int height);

		/**
		* Generates mipmaps for this texture array.
		*/
		void GenMipmap();

		/**
		* Gets the index in storage
		*/
		int GetIdxInStorage() { return imagesToStore.size() - 1; }

		/**
		* Returns height of the textures
		*/
		int GetHeight() const { return height; }

		/**
		* Returns the number of texture levels
		*/
		int GetSize() const { return size; }

		/**
		* Returns width of the textures
		*/
		int GetWidth() const { return width; }

		/**
		* Given image will be stored in texture storage when StoresImages function 
		* is called.
		*
		* @param image image that should be stored in texture storage
		*/
		void ImageToStore(std::string name, GLubyte *image);

		/**
		* Resets the storage i.e. deletes the images that were added to texture
		* array if they are not in texture cache. This also resets the index in
		* storage.
		*
		* @param texImgCacheNames vector that contains names of the images that
		* are in cache
		*/
		void ResetStorage(std::vector<std::string> &texImgCacheNames);

		/**
		* Stores images that were added using ImageToStore function into the 
		* texture array storage. Should be called after all the images were 
		* set to be stored. If the boolean is true it also creates a new 
		* array. If false it continues filling the existing texture array.
		*
		* @param additionalSpace additional space that should be taken into 
		* account when texture storage is set e.g. to account for images
		* in level 2 clusters one could set it to lvl2NumOfClusters
		* @param createArray creates an array as well if true
		*/
		void StoreImages(int additionalSpace, bool createArray = true);
		

	private:
		int width;
		int height;
		int size;

		int addedToStorageIdx;
		// Pair contains pointer the name of the image and the pointer to it
		std::vector<std::pair<std::string, GLubyte *> > imagesToStore;

		/**
		* Creates a texture array
		*/
		void createTextureArray();

	};


} /* end namespace msmcluster */
} /* end namespace megamol */

#endif /* MSMCLUSTER_TEXTUREARRAY_H_INCLUDED */