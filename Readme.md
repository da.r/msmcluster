# MSMCluster

MSMCluster is a plugin for [MegaMol](https://megamol.org/) that clusters and visualizes the clustering of molecular surface map images.

## Modules

### ClusterRenderer

Main module of the plugin. It clusters the images and uses force directed layout to visualize the clustering.

To start clustering, the user should click the _cluster_ button (or press _ENTER_). This starts the clustering with the selected clustering algorithm and the set number of clusters. The results are then visualized with representative images of every cluster in a force-directed layout. To see clusters within a cluster, the user should right-click on the representative image of the cluster. This starts clustering only with the images within the selected cluster. The results are then visualized with smaller representative images of those clusters within the outer cluster. These images interact with the representative image of the parent cluster using another instance of force directed layout.
Finally, to see all the images contained in a cluster, the user should click (with the left mouse button) on its representative image while holding down the _CTRL_ key. This will load all images within a cluster and display them using the uniform grid layout.

Zooming is controlled by holding down the middle mouse button and moving the mouse forward (to zoom in) or backward (to zoom out).

In order to speed up the clustering process, the module caches the feature vectors (image moments) of the images in the _cache\_gray.msmcluster_ or _cache\_colors.msmcluster_ files, depending on the number of color channels (1 or 3) used to compute the image moments. 

The module contains multiple parameters that allow the user to adjust it to their needs.
Depending on their functionality, parameters are put into different folders.
The module contains the following parameters: 
- Caching
	- textureCacheSize - Number of textures that can be cached.
- Clustering 
	- cluster button (_ENTER_) - Cluster the data again
	- clusteringAlgo - Allows the user to choose the clustering algorithm. The following algorithms can be chosen:
        - K-means
        - dlib_K-means
        - dlib_bottomUpAgglomerative - numberOfClusters parameter is interpreted as the minimal number of clusters
        - MeanShift - implemented in python, can only be used if the necessary submodule is initialized. To initialize and update it, one should execute the following two commands: (1) ```git submodule init```  (2) ```git submodule update ```
            - MeanShift finds the optimal number of clusters, numberOfClusters parameter is ignored when MeanShift is used
	- imagesPath - path to the folder that contains the images.
    - imageRepresentation - allows the user to choose the internal representation of the image used when clustering. The user can choose between image moments computed on all color channels, or on only one channel (grayscale representation).
	- numberOfClusters - data is clustered in this number of clusters.
	- numberOfClustersOnLvl2 - cluster is further clustered into this number of clusters.
- uniformGrid
	- back (_BACKSPACE_) - return to the force directed layout.
	- whichCluster - representative image of the cluster whose images are shown.
- visualization
	- imageSizeScalling - change the image size. Width and height of every image is multiplied by this value.
	- selectedImage - name of the image that is selected.
	- subclusterImageSize - scales every image that represents a cluster within a cluster with respect to the size of its parent cluster.
	- layout
		- currentEnergy - 1000000 x Current energy of the layout is shown here.
		- damping - damping factor applied to forces for quicker becalming of system.
		- stepLength - integration step length. This affects the movement speed of the images.
		- stopEnergy - 1000000 x Energy at which the layout calculation stops.
        
Remark: textureCacheSize should always be larger than the number of clusters

Example screenshot:
![ClusterRendererDemo.png](ClusterRendererDemo.png)

### ScatterPlotRenderer

Visualizes clustering with a scatter plot. Allows the user to see the distances between points in a 2D after dimensionality reduction has been applied to the image descriptors.
This module contains a subset of the parameters listed in the ClusterRenderer section.

Example screenshot:
![ScatterPlotRendererDemo.png](ScatterPlotRendererDemo.png)

**************
### Build

MSMCluster includes *dlib* library since it is required to build the plugin. Because of that, when compiling in debug configuration, one has to compile it with _/bigobj_ option. This [page](https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/ms173499(v=vs.100)) describes how to do this in visual studio.
The *stb_image* loader included in the project is used to load the images.

### Configuration in the MegaMol-Configurator 

To run this plugin in combination with the Tracking plugin in MegaMol, one has to set the module graph correctly.
For example for the interaction on the powerwall:
- Connect the View2D module with the ClusterRender module via a Call in the first slot. 
- Then connect the ClusterRender module from the second slot via a Call with the NatNetTracker module and 
connect the NatNetTracker module back to the ClusterRender module in the second slot.

![ModuleGraph.png](ModuleGraph.png)